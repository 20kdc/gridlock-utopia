#!/usr/bin/env python3

# Station library

import struct

DC_HMD = 0
DC_CONTROLLER = 1
DC_TRACKER = 2

DF_NULL = 1
DF_POSTRACK = 2
DF_ROTTRACK = 4
DF_CONTROLLER_LEFT = 8
DF_CONTROLLER_RIGHT = 16

def build_hmd_info_calc_ratio(res_w = 1280, res_h = 800, size_w = 0.149760, size_h = 0.93600, lens_sep = 0.063500, lens_vpos = 0.046800, fov_radians = 2.1906395386651707):
	return build_hmd_info(res_w, res_h, size_w, size_h, lens_sep, lens_vpos, fov_radians, (res_w / res_h) / 2.0)

def build_hmd_info(res_w, res_h, size_w, size_h, lens_sep, lens_vpos, fov_radians, ratio):
	return struct.pack("IIffffff", res_w, res_h, size_w, size_h, lens_sep, lens_vpos, fov_radians, ratio)

HMD_DISTORTION_NULL = struct.pack("ffffff", 0, 0, 0, 0, 0, 0)

CONTROL_INFO_NULL = struct.pack("II", 0, 0)

class Station:
	def __init__(self, cls, flg, name, controls, hmd_info, hmd_distortion = HMD_DISTORTION_NULL):
		# could crop a UTF-8 char, oh well
		namebin = name.encode("utf8")[0:255]
		padded_name = namebin + (b"\0" * (256 - len(namebin)))
		assert len(padded_name) == 256
		self.header = struct.pack("III", 0, cls, flg) + padded_name
		self.rotation = struct.pack("Iffff", 0, 0, 0, 0, 0)
		self.position = struct.pack("fff", 0, 0, 0)
		# controls stuff
		self.control_header = struct.pack("I", len(controls))
		self.control_body = [struct.pack("IIf", 0, 0, 0)] * 64
		cidx = 0
		for control in controls:
			assert len(control) == 8
			self.control_body[cidx] = control + struct.pack("f", 0)
			cidx += 1
		# hmd yada yada
		self.hmd_info = hmd_info
		self.hmd_distortion = hmd_distortion

	def set_rot_euler(self, pitch, yaw, roll):
		self.rotation = struct.pack("Iffff", 0, pitch, yaw, roll, 0)

	def set_rot_quat(self, x, y, z, w):
		self.rotation = struct.pack("Iffff", 1, x, y, z, w)

	def set_pos(self, x, y, z):
		self.position = struct.pack("fff", x, y, z)

	def set_control(self, cidx, val):
		self.control_body[cidx] = self.control_body[cidx][:8] + struct.pack("f", val)

	def build(self):
		return self.header + self.rotation + self.position + self.hmd_info + self.hmd_distortion + self.control_header + (b"".join(self.control_body))

