#!/bin/sh
# Personal: HMD (Hua.) with Wiimote replacing tablet functions
mkdir -p /run/user/1000/app/com.valvesoftware.Steam
./station3.py --gravity-smooth 4.0 --gravity /gridlock/android.sensor.accelerometer/0 --gyroscope /gridlock/android.sensor.gyroscope/5 --recentre-decay 0.99999 \
--type hmd --name hmd --lens-sep 0.055 \
--position /wmx/position \
--recentre-method /wmx/recentre \
--port 8999 --file $OPENHMD_FILEDEV_0
