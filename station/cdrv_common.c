/* Common code for pure-accelerometer secondary controller drivers */

#include "cdrv_common.h"

#include <lo/lo.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define ACC_FILTER_CONST 0.9

cdrv_t cdrv;

struct {
	float acc[3];
	uint16_t buttons_last;
	uint16_t buttons_pressed;
} cdrv_filtered;

struct {
	float vcsx;
	float vcsy;
	float vcsPos[3];
	int zy_plane;
} cdrv_state;

struct {
	float pos[3];
} cdrv_oscout;

void cdrv_update() {
	// update filter
	for (int i = 0; i < 3; i++)
		cdrv_filtered.acc[i] = (cdrv_filtered.acc[i] * cdrv.acc_filter_const) + (cdrv.acc[i] * (1.0 - cdrv.acc_filter_const));
	cdrv_filtered.buttons_pressed = cdrv.buttons & ~cdrv_filtered.buttons_last;
	cdrv_filtered.buttons_last = cdrv.buttons;
	// "virtual cursor" workings-out
	float vcx = atan2f(cdrv_filtered.acc[CDRV_AXIS_RIGHT], cdrv_filtered.acc[CDRV_AXIS_DOWN]);
	float vcy = -atan2f(cdrv_filtered.acc[CDRV_AXIS_FORWARD], cdrv_filtered.acc[CDRV_AXIS_DOWN]);
	// check buttons
	if (cdrv_filtered.buttons_pressed & CDRV_BUTTON_RECENTRE) {
		if (lo_send(cdrv.osc_target, "/wmx/recentre", "") == -1) {
			fprintf(stderr, "OSC ERROR, can't recentre\n");
		}
		cdrv_oscout.pos[0] = 0;
		cdrv_oscout.pos[1] = 0;
		cdrv_oscout.pos[2] = 0;
	}
	if (cdrv_filtered.buttons_pressed & CDRV_BUTTON_IDR) {
		system("./sunshine-pls");
	}
	if (cdrv_filtered.buttons_pressed & (CDRV_BUTTON_DRAWING | CDRV_BUTTON_MOVING)) {
		cdrv_state.vcsx = vcx;
		cdrv_state.vcsy = vcy;
		for (int i = 0; i < 3; i++)
			cdrv_state.vcsPos[i] = cdrv_oscout.pos[i];
	}
	// movement common
	if (cdrv.buttons & (CDRV_BUTTON_DRAWING | CDRV_BUTTON_MOVING)) {
		for (int i = 0; i < 3; i++)
			cdrv_oscout.pos[i] = cdrv_state.vcsPos[i];
		if ((cdrv.buttons & (CDRV_BUTTON_DRAWING | CDRV_BUTTON_MOVING)) == (CDRV_BUTTON_DRAWING | CDRV_BUTTON_MOVING)) {
			cdrv_state.zy_plane = 1;
		}
	} else {
		cdrv_state.zy_plane = 0;
	}
	// movement specific
	if (cdrv_state.zy_plane) {
		cdrv_oscout.pos[2] += vcx - cdrv_state.vcsx;
		cdrv_oscout.pos[1] += vcy - cdrv_state.vcsy;
	} else if (cdrv.buttons & CDRV_BUTTON_DRAWING) {
		cdrv_oscout.pos[0] += vcx - cdrv_state.vcsx;
		cdrv_oscout.pos[1] += vcy - cdrv_state.vcsy;
	} else if (cdrv.buttons & CDRV_BUTTON_MOVING) {
		cdrv_oscout.pos[0] += vcx - cdrv_state.vcsx;
		cdrv_oscout.pos[2] += vcy - cdrv_state.vcsy;
	}
	// transmit
	if (lo_send(cdrv.osc_target, "/wmx/position", "fff", cdrv_oscout.pos[0], cdrv_oscout.pos[1], cdrv_oscout.pos[2]) == -1) {
		fprintf(stderr, "OSC ERROR, can't position\n");
	}
	// monitor
	fprintf(stderr, "%f %f | %f %f %f | %f %f %f | %i\n", vcx, vcy, cdrv_filtered.acc[0], cdrv_filtered.acc[1], cdrv_filtered.acc[2], cdrv_oscout.pos[0], cdrv_oscout.pos[1], cdrv_oscout.pos[2], (int) cdrv.battery);
}
