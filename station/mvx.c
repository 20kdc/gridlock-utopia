// Connector to connect move to station3
#include <psmoveapi/psmove.h>
#include "cdrv_common.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#define ACC_FILTER_CONST 0.9

static void main_loop() {
	PSMove * pm = psmove_connect();
	if (!pm) {
		fprintf(stderr, "<*MVX> failed to connect to PSMove\n");
		return;
	}
	fprintf(stderr, "<*MVX> connected to PSMove\n");
	// I think this is pulling too much power, sadly
	/*
	// LED test :D
	int ts[5] = {0, 1, 2, 1, 0};
	for (int i = 0; i < 1; i++) {
		for (int j = 0; j < 5; j ++) {
			switch (ts[j]) {
				case 0: psmove_set_leds(pm, 0, 255, 255); break;
				case 1: psmove_set_leds(pm, 255, 0, 255); break;
				case 2: psmove_set_leds(pm, 255, 255, 255); break;
			}
			psmove_update_leds(pm);
			usleep(1000000);
		}
	}
	*/
	time_t last_report_time = time(NULL);
	while (1) {
		usleep(10000);
		int seq = psmove_poll(pm);
		if (seq != 0) {
			last_report_time = time(NULL);
		} else if (time(NULL) > (last_report_time + 2)) {
			// !!! oh no, retry !!!
			break;
		}
		psmove_set_leds(pm, 0, 0, 0);
		psmove_update_leds(pm);

		cdrv.battery = psmove_get_battery(pm);

		// adapt accelerometer data nicely
		psmove_get_accelerometer_frame(pm, Frame_SecondHalf, cdrv.acc + CDRV_AXIS_RIGHT, cdrv.acc + CDRV_AXIS_FORWARD, cdrv.acc + CDRV_AXIS_DOWN);
		cdrv.acc[CDRV_AXIS_RIGHT] *= -1;
		cdrv.acc[CDRV_AXIS_FORWARD] *= -1;

		// roughly match the layout of the Wiimote setup
		// like the Wiimote we also have a ton of extra buttons we have no idea what to do with
		// if I'm being honest? this is a holdover until we figure out how to get the camera setup and *really* make use of this properly
		// CDRV is just meant to be a way to turn generic accelerometer+buttons devices into functional movement anchors so we can adapt whatever we have to for meetups in a hurry
		// also, you, yes, YOU, the one with the stuffed dog hat! sina lukin tawa ni la, sina epiku

		unsigned int pmb = psmove_get_buttons(pm);
		cdrv.buttons = 0;
		if (pmb & Btn_MOVE)
			cdrv.buttons |= CDRV_BUTTON_DRAWING;
		if (pmb & Btn_T)
			cdrv.buttons |= CDRV_BUTTON_MOVING;
		if (pmb & Btn_CROSS)
			cdrv.buttons |= CDRV_BUTTON_RECENTRE;
		if (pmb & Btn_CIRCLE)
			cdrv.buttons |= CDRV_BUTTON_IDR;

		cdrv_update();
	}
}

int main(int argc, char ** argv) {
	if (argc != 2) {
		fprintf(stderr, "mvx LOTARGET\n");
		return 1;
	}
	if (!psmove_init(PSMOVE_CURRENT_VERSION)) {
		fprintf(stderr, "failed to init psmoveapi\n");
		return 1;
	}
	cdrv.acc_filter_const = ACC_FILTER_CONST;
	cdrv.osc_target = lo_address_new_from_url(argv[1]);
	if (!cdrv.osc_target) {
		fprintf(stderr, "<*MVX> invalid liblo URL %s\n", argv[1]);
		return 1;
	}
	while (1) {
		fprintf(stderr, "<*MVX> start (locating move)\n");
		main_loop();
	}
	return 0;
}
