// Connector to connect wiimote to station3
#include <bluetooth/bluetooth.h>
#include <cwiid.h>
#include "cdrv_common.h"
#include <unistd.h>
#include <stdlib.h>
#include <math.h>

#define ACC_FILTER_CONST 0.9

struct {
	bdaddr_t wiimote;
	float axis_offsets[3];
} prg_args;

static void cb(cwiid_wiimote_t *wm, int mc, union cwiid_mesg * mv, struct timespec *timestamp) {
	float axis_actual[3];
	while (mc > 0) {
		switch (mv->type) {
			case CWIID_MESG_BTN:
				cdrv.buttons = 0;
				if (mv->btn_mesg.buttons & CWIID_BTN_MINUS)
					cdrv.buttons |= CDRV_BUTTON_RECENTRE;
				if (mv->btn_mesg.buttons & CWIID_BTN_PLUS)
					cdrv.buttons |= CDRV_BUTTON_IDR;
				if (mv->btn_mesg.buttons & CWIID_BTN_A)
					cdrv.buttons |= CDRV_BUTTON_DRAWING;
				if (mv->btn_mesg.buttons & CWIID_BTN_B)
					cdrv.buttons |= CDRV_BUTTON_MOVING;
				break;
			case CWIID_MESG_ACC:
				for (int i = 0; i < 3; i++)
					axis_actual[i] = ((float) mv->acc_mesg.acc[i]) - prg_args.axis_offsets[i];
				cdrv.acc[CDRV_AXIS_RIGHT] = axis_actual[0];
				cdrv.acc[CDRV_AXIS_FORWARD] = axis_actual[1];
				cdrv.acc[CDRV_AXIS_DOWN] = axis_actual[2];
				break;
			case CWIID_MESG_STATUS:
				cdrv.battery = mv->status_mesg.battery;
			default:
				// nope
				break;
		}
		mc--;
		mv++;
	}
}

static void main_loop() {
	// cwiid scanning is borked and you can use bluetoothctl to figure this out
	bdaddr_t target = prg_args.wiimote;
	cwiid_wiimote_t * wm = cwiid_open(&target, CWIID_FLAG_MESG_IFC);
	if (!wm) {
		fprintf(stderr, "<*WMX> failed to connect to wiimote\n");
		return;
	}
	fprintf(stderr, "<*WMX> connected to wiimote\n");
	cwiid_set_rpt_mode(wm, CWIID_RPT_ACC | CWIID_RPT_BTN | CWIID_RPT_STATUS);
	cwiid_set_mesg_callback(wm, cb);
	cwiid_request_status(wm);
	while (1) {
		usleep(10000);
		if (cwiid_request_status(wm)) {
			break;
		}
		cdrv_update();
	}
}

int main(int argc, char ** argv) {
	if (argc != 6) {
		fprintf(stderr, "wmx BTADDR OX OY OZ LOTARGET\n");
		return 1;
	}
	if (str2ba(argv[1], &prg_args.wiimote)) {
		fprintf(stderr, "invalid bluetooth address: %s\n", argv[1]);
		return 1;
	}
	prg_args.axis_offsets[0] = atof(argv[2]);
	prg_args.axis_offsets[1] = atof(argv[3]);
	prg_args.axis_offsets[2] = atof(argv[4]);
	cdrv.acc_filter_const = ACC_FILTER_CONST;
	cdrv.osc_target = lo_address_new_from_url(argv[5]);
	if (!cdrv.osc_target) {
		fprintf(stderr, "<*WMX> invalid liblo URL %s\n", argv[5]);
		return 1;
	}
	while (1) {
		fprintf(stderr, "<*WMX> start (locating wiimote)\n");
		main_loop();
	}
	return 0;
}
