#!/bin/sh
# Personal: left controller
./station3.py --gravity-smooth 4.0 --gravity /nove/android.sensor.accelerometer/1 --gyroscope /nove/android.sensor.gyroscope/4 --recentre-decay 0.999 \
--type controller-l --name controller-l \
--position /nove/the_hand \
--port 8998 --file $OPENHMD_FILEDEV_1
