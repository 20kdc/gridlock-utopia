#!/usr/bin/env python3

# Updates an OpenHMD filedev file ( https://github.com/20kdc/OpenHMD/tree/drv-file ) based on incoming OSC data.
# Also checks for a "recentre file". This sets the "zero yaw".

import sys
import os
import struct
import math
import argparse
import time
import liblo
import libstation

try:
	import signal
	signal.signal(signal.SIGINT, signal.SIG_DFL)
except:
	pass

parser = argparse.ArgumentParser(prog="station3.py", description="gridlock-utopia:pivot Station server")
parser.add_argument("--port", help="UDP port to receive data on", default=9000)
parser.add_argument("--type", help="Type", choices=["hmd", "controller-l", "controller-r"], default="hmd")
parser.add_argument("--name", help="Name", default="gridlock-utopia")
parser.add_argument("--file", help="File to write to")

# use Rift emulation values for consistency
# these two should be considered **locked**
parser.add_argument("--res-width", help="screen width in pixels", default=2160)
parser.add_argument("--res-height", help="screen height in pixels", default=1200)
parser.add_argument("--phys-width", help="physical width in metres (usually estimated from height & aspect assuming square pixels)")
parser.add_argument("--phys-height", help="physical height in metres", default=0.06)
parser.add_argument("--lens-sep", help="lens separation in metres (usually estimated: half physical width)")
parser.add_argument("--lens-vpos", help="lens vertical position in metres (usually estimated: half physical height)")
parser.add_argument("--fov", help="FOV in degrees", default=90)

parser.add_argument("--gyroscope", help="Gyroscope sensor")
parser.add_argument("--gravity", help="Accelerometer or Gravity sensor")
parser.add_argument("--gravity-smooth", help="Smooth accelerometer/gravity sensor by 1 frame with this factor")
parser.add_argument("--position", help="Position sensor (likely IMS)")
parser.add_argument("--the-hand", help="Position from The Hand")
parser.add_argument("--the-hand-scale", help="Scale for The Hand", default = 2.0)

parser.add_argument("--recentre-file", help="Recentre file - create to recentre yaw (will be deleted)")
parser.add_argument("--recentre-method", help="Recentre method - OSC 'void method' to recentre yaw")
parser.add_argument("--recentre-decay", help="Recentring via gradual decay, (processed per input tick, sorry) measured as yaw difference multiplier per input tick", default = 1.0)

parser.add_argument("--button", help="Adds a digital button. No hint is provided since you'll just have to rebind it anyway", default=[], action="append")

args = parser.parse_args()

# start with resolution
res_width = int(args.res_width)
res_height = int(args.res_height)
# get physical height
phys_height = float(args.phys_height)
# estimate physical width from physical height
if args.phys_width is None:
	phys_width = phys_height * float(res_width) / float(res_height)
else:
	phys_width = float(args.phys_width)
# estimate lens sep
if args.lens_sep is None:
	lens_sep = phys_width / 2.0
else:
	lens_sep = float(args.lens_sep)
# estimate lens vpos
if args.lens_vpos is None:
	lens_vpos = phys_height / 2.0
else:
	lens_vpos = float(args.lens_vpos)

print(str(res_width) + "x" + str(res_height) + "px, " + str(phys_width) + "x" + str(phys_height) + "m, sep " + str(lens_sep) + "m, vpos " + str(lens_vpos) + "m")

hmd_info = libstation.build_hmd_info_calc_ratio(res_width, res_height, phys_width, phys_height, lens_sep, lens_vpos, math.radians(float(args.fov)))

device_class = libstation.DC_HMD
device_flags = libstation.DF_ROTTRACK
if not ((args.position is None) and (args.the_hand is None)):
	device_flags |= libstation.DF_POSTRACK

if args.type == "controller-l":
	device_class = libstation.DC_CONTROLLER
	device_flags |= libstation.DF_CONTROLLER_LEFT
elif args.type == "controller-r":
	device_class = libstation.DC_CONTROLLER
	device_flags |= libstation.DF_CONTROLLER_RIGHT

info_buttons = []

for v in args.button:
	info_buttons.append(libstation.CONTROL_INFO_NULL)

station = libstation.Station(device_class, device_flags, args.name, info_buttons, hmd_info)

fn = args.file
fn_recentre = args.recentre_file

try:
	f = open(fn, "x+b")
except:
	f = open(fn, "r+b")

gyroscope_total = 0.0

def poll_recentre():
	try:
		os.unlink(fn_recentre)
		return True
	except:
		return False

def set_vals(pitch, yaw, roll, x, y, z):
	global gyroscope_total
	if poll_recentre():
		gyroscope_total = 0
	gyroscope_total *= float(args.recentre_decay)
	station.set_rot_euler(pitch, yaw, roll)
	station.set_pos(x, y, z)
	f.seek(0)
	f.write(station.build())
	f.flush()

set_vals(0, 0, 0, 0, 0, 0)

port_int = int(args.port)
srv = liblo.Server(port_int, liblo.UDP)
print("Station: " + args.name + ": UDP " + str(port_int))

gyroscope_last_at = time.time()
gravity = [0.0, 0.0, 0.0]
position = [0.0, 0.0, 0.0]
was_meaningful = False

if not args.gyroscope is None:
	def record_gyroscope(path, data, types):
		global gyroscope_last_at, gyroscope_total, was_meaningful
		was_meaningful = True
		newtime = time.time()
		diff = gyroscope_last_at - newtime
		gyroscope_last_at = newtime
		gyroscope_total -= data[0] * diff
	srv.add_method(args.gyroscope, "fff", record_gyroscope)
if not args.gravity is None:
	def record_gravity(path, data, types):
		global gravity, was_meaningful
		was_meaningful = True
		if not args.gravity_smooth is None:
			gsf = float(args.gravity_smooth)
			gsft = gsf + 1.0
			gravity = [
				((gravity[0] * gsf) + data[0]) / gsft,
				((gravity[1] * gsf) + data[1]) / gsft,
				((gravity[2] * gsf) + data[2]) / gsft,
			]
		else:
			gravity = [data[0], data[1], data[2]]
	srv.add_method(args.gravity, "fff", record_gravity)
if not args.position is None:
	def record_position(path, data, types):
		global position, was_meaningful
		was_meaningful = True
		position = [data[0], data[1], data[2]]
	srv.add_method(args.position, "fff", record_position)
if not args.the_hand is None:
	last_hand_time = time.time()
	def record_the_hand(path, data, types):
		global position, was_meaningful, gyroscope_total, last_hand_time
		current_hand_time = time.time()
		delta_time = current_hand_time - last_hand_time
		last_hand_time = current_hand_time
		was_meaningful = True
		# Assuming top of portrait is on left side.
		pen_writing_mode = False
		if data[0] > 0.9:
			print("Pen writing mode")
			pen_writing_mode = True
		else:
			if data[1] < -0.9:
				print("G-")
				gyroscope_total += delta_time * 0.125
			if data[1] > 0.9:
				print("G+")
				gyroscope_total -= delta_time * 0.125
		ths = float(args.the_hand_scale)
		if pen_writing_mode:
			position = [
				# left/right
				data[3] * ths,
				# up/down
				data[2] * ths,
				# forward/back
				0
			]
		else:
			position = [
				# left/right
				data[3] * ths,
				# up/down
				data[0] * ths,
				# forward/back
				data[2] * -ths
			]
	srv.add_method(args.the_hand, "ffff", record_the_hand)
if not args.recentre_method is None:
	def record_recentre_method(path, data, types):
		global gyroscope_total
		gyroscope_total = 0
		was_meaningful = True
	srv.add_method(args.recentre_method, "", record_recentre_method)

button_index = 0
for button in args.button:
	def make_record_button(cidx):
		def record_button(path, data, types):
			global was_meaningful
			station.set_control(cidx, data[0])
			was_meaningful = True
		return record_button
	srv.add_method(button, "f", make_record_button(button_index))
	button_index += 1

while True:
	srv.recv()
	if not was_meaningful:
		continue
	was_meaningful = False
	pitch = 0.0
	yaw = 0.0
	roll = 0.0
	if not args.gravity is None:
		# Pitch is on the -Z+X plane
		pitch = math.atan2(-gravity[2], gravity[0])
		# Roll is on the -Y+X plane
		roll = math.atan2(-gravity[1], gravity[0])
		pass
	if not args.gyroscope is None:
		# hmm
		yaw = gyroscope_total
		pass
	print("S", gyroscope_total, "G", gravity[0], gravity[1], gravity[2], "R", pitch, yaw, roll)
	print("P", position)
	set_vals(pitch, yaw, roll, position[0], position[1], position[2])
