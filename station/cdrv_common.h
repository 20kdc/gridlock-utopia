#pragma once

/* Common code for pure-accelerometer secondary controller drivers */

#include <stdint.h>
#include <lo/lo.h>

#define CDRV_BUTTON_DRAWING 1
#define CDRV_BUTTON_MOVING 2
#define CDRV_BUTTON_RECENTRE 4
#define CDRV_BUTTON_IDR 8

// Names defined relative to a controller lying facing forward on a flat surface.
#define CDRV_AXIS_RIGHT 0
#define CDRV_AXIS_FORWARD 1
#define CDRV_AXIS_DOWN 2

typedef struct {
	lo_address osc_target;
	// CDRV_BUTTON_*
	uint16_t buttons;
	uint8_t battery;
	// Accelerometer. An approximation of gravity.
	// In arbitrary units.
	// See CDRV_AXIS_* constants.
	float acc[3];
	float acc_filter_const;
} cdrv_t;

extern cdrv_t cdrv;

void cdrv_update();
