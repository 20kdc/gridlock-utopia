# SteamVR-OpenHMD Modifications

Here's how to rig <https://github.com/ChristophHaag/SteamVR-OpenHMD> for use with this + Flatpak

## Step 1: Correct Branches

**Use <https://github.com/20kdc/SteamVR-OpenHMD/tree/disable-autodetect-if-config-provided> (unless it got upstreamed).**
This is necessary because otherwise your config changes will be ignored.

**In addition, after recursive cloning, you must change out the openhmd submodule to use <https://github.com/20kdc/OpenHMD/tree/drv-file>.** This is necessary for communication between OpenHMD and the station.

## Step 2: Remove hidapi use from OpenHMD so it'll load in the flatpak

In subprojects/openhmd/meson_options.txt you should see a description for the "drivers" option.

Change the default value so only the "file" driver remains.

This will cause hidapi to not get linked which will make the resulting binary more able to survive flatpak use.

## Step 3: Config

Prepare your `~/.ohmd_config.txt` to prevent it using null devices for controllers:

```
hmddisplay 0
hmdtracker 0
leftcontroller -1
rightcontroller -1
```

This must also be copied to `~/.var/app/com.valvesoftware.Steam/.ohmd_config.txt` for the Flatpak version.

## Step 4: Compile the driver

Build with `mkdir build; meson build; cd build; ninja` or w/e then `cd ..` and  `./install_files_to_build.sh` as usual.

Congratulations you now have the OpenVR driver, you need to inject it into your Steam install now

`flatpak run --command=bash com.valvesoftware.Steam` is your friend here and will let you do things like mess with `vrpathreg`

Remember these tips:

* You can copy your entire SteamVR-OpenHMD directory into `/home/t20kdc/.var/app/com.valvesoftware.Steam` so you can use `register.sh` as normal
* DO NOT FORGET TO COMPILE *OR* TO `./install_files_to_build.sh` WHEN UPDATING CODE (both are required yaaaaaaaaaaay for no symliiiiinks)
* When planning your filedev keep in mind that `/run/user/1000/app/com.valvesoftware.Steam/` (1000 should be whatever your UID is) is the same place both inside and outside the flatpak.
* Just to clarify: DO: `export OPENHMD_FILEDEV_0=/run/user/1000/app/com.valvesoftware.Steam/openhmd` DON'T: `/run/ketesi-this-will-not-enter-or-leave-flatpak`
* **USE THE BASH-IN-FLATPAK COMMAND ABOVE TO CHECK AND DOUBLECHECK EVERYTHING**

