# This Thing Needs A Better Name

## Notice (Dec. 2024)

**The mechanisms in this repository are now no longer what I use.** The OSC graph editor might end up used for something, but otherwise nope. _**SteamVR basically screwed everything up**_ and I wasn't able to get OpenComposite working with my patched Monado/OpenHMD setup. I am now looking into a WiVRn based setup; being a "known" process might help.

**_The code here will remain available for anyone who needs it._**

If you're thinking of continuing this project: _Please, figure out how to make [PhoneVR](https://github.com/PhoneVR-Developers/PhoneVR) work on more phones instead, ideally by borrowing from whatever Moonlight/Sunshine are doing._ I don't have the encoder chops to do it but I'm sure *someone* does.

This entire repository is a big nasty workaround because I couldn't just do that, and it shows.

## Rambling

This probably should have been migrated to scrapheap at some point but I don't care.

I now have the hardware to try and do this semi-properly.

## If You Need The Legacy Version...

If you need the 'legacy version' -- presumably for the multitouch support because nothing else was ever really finished -- see commit `a821a58e75c9d424becaa5ff4752fbda1d73a314`.

However, keep in mind that [Moonlight](https://moonlight-stream.org/) & [Sunshine](https://github.com/LizardByte/Sunshine) are just plain better in most ways -- the only, i.e. singular exception is that they don't have multitouch support or pressure support. However, in practice, it is a much better experience for drawing, owing to the better latency.

## Before You Begin

Attempt <https://github.com/PhoneVR-Developers/PhoneVR> *first.* This may not work because of SteamVR weirdness, or because of Android weirdness (the reason I can't use it has been hypothesized to be a codec issue). But attempt it first.

## Requirements & Where To Look For Instructions

* An Android device you would like to strap to your head.
	* It at minimum requires an accelerometer. **A gyroscope is not essential, but highly recommended.**
* A gamepad (ideally "xbox/playstation-ish"), properly configured for Steam Input.
	* SteamVR does not comprehend keyboards and mice.
* An appropriate "Cardboard-style" lens assembly.
	* **Actual Cardboard compliance is not necessary. Having lenses and the positioning is enough.** Testing for this project was done using a botched headset not intended for Cardboard compliance at all, that was lacking the touch-arm.
* An unused HDMI (DisplayPort is untested) connector. *No device has to be, or in fact should be, attached to this connector.*
* Some Linux distribution. Yes, this is Linux-only. There are good reasons for this.
	* I had to switch to Wayland to make this work. This is funny because it's contrary to most people's experiences where using Wayland breaks everything.
* Above all else: **Root access and a willingness to do terrible things in the name of science.**

Assuming you meet these requirements, congratulations, move onto [the instructions](INSTRUCTIONS.md).
