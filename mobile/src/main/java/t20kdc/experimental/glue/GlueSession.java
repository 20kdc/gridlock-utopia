package t20kdc.experimental.glue;

import android.content.Context;
import t20kdc.experimental.glue.cfg.GlueConfig;
import t20kdc.experimental.glue.extdev.Detection;
import t20kdc.experimental.glue.extdev.ExternalSensor;
import t20kdc.experimental.glue.extdev.GmAndroidSensorSetupService;
import t20kdc.experimental.glue.extdev.OutOfSessionRepeatableGlobals;
import t20kdc.experimental.glue.svc.*;

import java.net.InetAddress;
import java.net.InetSocketAddress;

public class GlueSession {
    public final GlueConfig config;
    public final OutOfSessionRepeatableGlobals globals;
    public GnBeaconService beacon;

    public GlueSession(Context c, GlueConfig cfg) {
        globals = new OutOfSessionRepeatableGlobals(c);
        config = cfg;
    }

    void addBasicIO(GiServiceManager svc) {
        InetAddress txAddress;
        try {
            txAddress = InetAddress.getByName(config.targetAddress);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        GsPacketTransmissionService transmitter = svc.add(new GsPacketTransmissionService(new InetSocketAddress(txAddress, config.targetPort)));
        beacon = svc.add(new GnBeaconService(svc, transmitter, config.name));
    }

    void addExternalSensors(GiServiceManager svc) {
        svc.add(new GmAndroidSensorSetupService(globals));
        for (ExternalSensor es : Detection.scan(globals))
            if (es.hasPermission())
                if (config.externalSensorEnable.contains(es.getUniqueId()))
                    es.open(svc, beacon, config.cVec);
    }
}
