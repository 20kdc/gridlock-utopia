package t20kdc.experimental.glue.svc;

import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * GiServiceManager holds the services used when the GluePrimaryActivity is running.
 */
public class GiServiceManager {
    private final LinkedList<GiServiceState> services = new LinkedList<>();
    // this is pre-multithread & as such MUST NOT BE TURNED OFF
    private boolean noLongerSafeToAddServices = false;

    // We don't startup or shutdown while this lock is held by anything other than the startup/shutdown function
    private ReentrantLock startupShutdownLock = new ReentrantLock();
    private boolean running;
    private final GiListener<Exception> errorHandler;

    public GiServiceManager(GiListener<Exception> eh) {
        errorHandler = eh;
    }

    public <T extends GiService> T add(T svc) {
        if (noLongerSafeToAddServices)
            throw new RuntimeException("it is no longer safe to add services");
        services.add(new GiServiceState(svc, errorHandler));
        return svc;
    }

    public <T extends GiService> T getService(Class<T> clz) {
        // ... How is this unchecked?
        for (GiServiceState gss : services)
            if (clz.isInstance(gss.service))
                return (T) gss.service;
        return null;
    }

    // -- Post-Setup --

    public void setStatus(boolean r) {
        startupShutdownLock.lock();
        noLongerSafeToAddServices = true;
        boolean start = r && !running;
        boolean stop = running && !r;
        // If starting, turn on running immediately so services can immediately start forwarding each other tasks
        // This is needed for sensor registration.
        // If stopping, turn off running immediately because stopped services can't run tasks
        running = r;
        for (GiServiceState gss : services) {
            if (start)
                gss.start();
            if (stop)
                gss.stop();
        }
        startupShutdownLock.unlock();
    }

    // Used by services receiving callbacks.
    public void runOnServiceThread(GiService gs, Runnable r) {
        startupShutdownLock.lock();
        if (!running) {
            startupShutdownLock.unlock();
            throw new RuntimeException("Wasn't running but tried to submit task to service");
        }
        for (GiServiceState gss : services) {
            if (gss.service == gs) {
                gss.sendTask(r);
                startupShutdownLock.unlock();
                return;
            }
        }
        startupShutdownLock.unlock();
        throw new RuntimeException("Unable to find service (wha?)");
    }
}
