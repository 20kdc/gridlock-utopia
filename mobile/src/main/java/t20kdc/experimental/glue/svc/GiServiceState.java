package t20kdc.experimental.glue.svc;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class GiServiceState implements Runnable {
    public final GiService service;
    private AtomicBoolean halt = new AtomicBoolean();
    private ConcurrentLinkedQueue<Runnable> tasks = new ConcurrentLinkedQueue<>();
    private Thread thread;
    private boolean actuallyReallyRunning = false;
    private final GiListener<Exception> errorHandler;

    public GiServiceState(GiService svc, GiListener<Exception> eh) {
        service = svc;
        errorHandler = eh;
    }

    public void sendTask(Runnable r) {
        tasks.add(r);
    }

    @Override
    public void run() {
        try {
            service.svcStart(true);
            while (!halt.get()) {
                while (true) {
                    Runnable next = tasks.poll();
                    if (next != null) {
                        next.run();
                    } else {
                        break;
                    }
                }
                service.svcLoop();
            }
        } catch (Exception e) {
            errorHandler.receive(e);
        }
        try {
            service.svcEnd(true);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void start() {
        try {
            service.svcStart(false);
        } catch (Exception e) {
            errorHandler.receive(e);
            try {
                service.svcEnd(false);
                return;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (service.svcHasThread()) {
            if (thread == null) {
                halt.set(false);
                thread = new Thread(this);
                thread.start();
                actuallyReallyRunning = true;
            } else {
                throw new RuntimeException("Invalid state (GiServiceManager should make this impossible. You broke it, didn't you.)");
            }
        }
    }

    public void stop() {
        if (!actuallyReallyRunning)
            return;
        if (service.svcHasThread()) {
            if (thread != null) {
                halt.set(true);
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    throw new RuntimeException("Interrupted during service stop", e);
                }
                thread = null;
                tasks.clear();
            }
        }
        try {
            service.svcEnd(false);
        } catch (Exception e) {
            errorHandler.receive(e);
        }
        actuallyReallyRunning = false;
    }
}
