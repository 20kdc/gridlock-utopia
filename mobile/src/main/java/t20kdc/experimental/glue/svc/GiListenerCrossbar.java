package t20kdc.experimental.glue.svc;

import java.util.LinkedList;

/**
 * An event crossbar. Used for routing.
 * @param <T> the event type
 */
public class GiListenerCrossbar<T> implements GiListener<T> {
    private LinkedList<GiListener<T>> listeners = new LinkedList<>();

    public void add(GiListener<T> listener) {
        listeners.add(listener);
    }

    @Override
    public void receive(T value) {
        for (GiListener<T> l : listeners)
            l.receive(value);
    }
}
