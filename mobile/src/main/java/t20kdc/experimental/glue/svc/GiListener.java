package t20kdc.experimental.glue.svc;

public interface GiListener<T> {
    void receive(T value);
}
