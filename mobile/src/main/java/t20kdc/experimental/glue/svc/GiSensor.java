package t20kdc.experimental.glue.svc;

public interface GiSensor {
    // A name that should be (relatively) unique and also human-readable.
    // Also must be a valid OSC Method.
    String getUniqueName();
    // The last values from the sensor (may have a length of 0 if no values have been received yet)
    // Warning: reference to received buffer, do not change content
    float[] getLastValues();
}
