package t20kdc.experimental.glue.svc;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;

public class GnBeaconService implements GiService, GiListener<GiSensor> {
    private final GiServiceManager svc;
    private final GiListener<byte[]> tx;
    private final LinkedList<GiSensor> sensors = new LinkedList<>();
    private final String beaconName;

    public GnBeaconService(GiServiceManager m, GiListener<byte[]> pts, String nam) {
        svc = m;
        tx = pts;
        beaconName = nam;
    }

    @Override
    public void receive(final GiSensor gs) {
        svc.runOnServiceThread(this, () -> sensors.add(gs));
    }

    @Override
    public boolean svcHasThread() {
        return true;
    }

    @Override
    public void svcStart(boolean onThread) {
        if (!onThread)
            return;
        // note that the service thread callbacks can't have possibly started running yet
        sensors.clear();
    }

    private void writeOSCString(OutputStream os, String text) throws IOException {
        int tl = text.length();
        int totalBytes = 0;
        for (int i = 0; i < tl; i++) {
            int val = text.charAt(i) & 0xFF;
            if (val == 0)
                continue;
            os.write(val);
            totalBytes++;
        }
        os.write(0);
        totalBytes++;
        writePadding(os, totalBytes);
    }
    private void writePadding(OutputStream os, int totalBytes) throws IOException {
        while ((totalBytes & 3) != 0) {
            os.write(0);
            totalBytes++;
        }
    }

    private void writeOSCBundle(DataOutputStream os, LinkedList<byte[]> elements) throws IOException {
        writeOSCString(os, "#bundle");
        os.writeLong(1);
        for (byte[] elm : elements) {
            os.writeInt(elm.length);
            os.write(elm);
        }
    }

    private void writeOSCMessage(DataOutputStream os, String method, float[] data) throws IOException {
        writeOSCString(os, method);
        os.write(',');
        for (int i = 0; i < data.length; i++)
            os.write('f');
        os.write(0);
        writePadding(os, data.length + 2);
        for (float elm : data)
            os.writeFloat(elm);
    }

    private byte[] makeOSCMessage(String method, float[] data) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        writeOSCMessage(new DataOutputStream(baos), method, data);
        return baos.toByteArray();
    }

    @Override
    public void svcLoop() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            // Nope, don't care.
        }
        try {
            // this could be written a lot better, but oh well
            LinkedList<byte[]> elements = new LinkedList<>();
            for (GiSensor data : sensors)
                elements.add(makeOSCMessage("/" + beaconName + "/" + data.getUniqueName(), data.getLastValues()));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);
            writeOSCBundle(dos, elements);
            dos.flush();
            tx.receive(baos.toByteArray());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void svcEnd(boolean onThread) {
    }
}
