package t20kdc.experimental.glue.svc;

public interface GiService {
    boolean svcHasThread();
    // the off-thread start/end functions are "wrapped around" the start/end of the thread
    // i.e. there's an implicit mutex
    void svcStart(boolean onThread);
    // this is run even if an exception occurs!
    void svcEnd(boolean onThread);
    // this is only for on-thread
    // between loops, scheduled tasks get executed; GsPeerTrackingService uses this intentionally
    void svcLoop();
}
