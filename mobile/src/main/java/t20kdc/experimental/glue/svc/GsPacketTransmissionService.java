package t20kdc.experimental.glue.svc;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.locks.ReentrantLock;

public class GsPacketTransmissionService implements GiService, GiListener<byte[]> {
    private final SocketAddress peer;
    private DatagramSocket socket;
    private final ReentrantLock lock = new ReentrantLock();

    public GsPacketTransmissionService(SocketAddress peer) {
        this.peer = peer;
    }

    @Override
    public boolean svcHasThread() {
        return false;
    }

    @Override
    public void svcStart(boolean onThread) {
        lock.lock();
        try {
            socket = new DatagramSocket(null);
            socket.setBroadcast(true);
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        lock.unlock();
    }

    @Override
    public void svcLoop() {
        // no thread
    }

    @Override
    public void svcEnd(boolean onThread) {
        lock.lock();
        socket.close();
        socket = null;
        lock.unlock();
    }

    @Override
    public void receive(final byte[] data) {
        lock.lock();
        try {
            if (socket != null)
                socket.send(new DatagramPacket(data, 0, data.length, peer));
        } catch (IOException e) {
            // nope, don't care
        }
        lock.unlock();
    }
}
