package t20kdc.experimental.glue.svc;

import android.app.Activity;
import t20kdc.experimental.glue.GlueErrorHandler;

public class GiServiceActivity extends Activity {
    public final GiServiceManager glueServices;

    public GiServiceActivity() {
        glueServices = new GiServiceManager(new GlueErrorHandler(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        glueServices.setStatus(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        glueServices.setStatus(false);
    }
}
