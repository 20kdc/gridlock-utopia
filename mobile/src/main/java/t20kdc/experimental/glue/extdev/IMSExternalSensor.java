package t20kdc.experimental.glue.extdev;

import android.app.PendingIntent;
import android.content.Context;
import android.hardware.Sensor;

import t20kdc.experimental.glue.cfg.CalibrationVector;
import t20kdc.experimental.glue.svc.GiListener;
import t20kdc.experimental.glue.svc.GiSensor;
import t20kdc.experimental.glue.svc.GiServiceManager;

public class IMSExternalSensor extends ExternalSensor {
    public final Sensor rotationVectorSensor;
    public final int rotationVectorSensorIndex;
    public final Sensor linearAccelerationSensor;
    public final int linearAccelerationSensorIndex;
    public IMSExternalSensor(Context ctx, Sensor rotationVectorSensor, int rotvecIndex, Sensor linearAccelerationSensor, int linacIndex) {
        super(ctx);
        this.rotationVectorSensor = rotationVectorSensor;
        this.rotationVectorSensorIndex = rotvecIndex;
        this.linearAccelerationSensor = linearAccelerationSensor;
        this.linearAccelerationSensorIndex = linacIndex;
    }

    @Override
    public String getUniqueId() {
        return "ims." + rotationVectorSensorIndex + "." + linearAccelerationSensorIndex;
    }

    @Override
    public String getDetectionReport() {
        return "Inertial Measurement System (" + rotationVectorSensor.getName() + "/" + linearAccelerationSensor.getName() + ")";
    }

    @Override
    public boolean hasPermission() {
        return true;
    }

    @Override
    public void askForPermission(PendingIntent returner) {

    }

    @Override
    public void open(GiServiceManager svc, GiListener<GiSensor> target, CalibrationVector cVec) {
        GmAndroidSensorSetupService as = svc.getService(GmAndroidSensorSetupService.class);
        svc.add(new GmInertialMeasurementSystem(as, "ims/" + rotationVectorSensorIndex + "/" + linearAccelerationSensorIndex, rotationVectorSensor, linearAccelerationSensor, target, cVec));
    }
}
