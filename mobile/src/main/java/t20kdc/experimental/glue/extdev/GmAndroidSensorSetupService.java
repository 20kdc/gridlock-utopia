package t20kdc.experimental.glue.extdev;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import java.util.LinkedList;

import t20kdc.experimental.glue.svc.GiListenerCrossbar;
import t20kdc.experimental.glue.svc.GiService;

/**
 * Central access point for sensors on Android.
 * This ensures that when multiple parts of the program want the same sensor, that can happen.
 */
public class GmAndroidSensorSetupService implements GiService, SensorEventListener {
    public final OutOfSessionRepeatableGlobals globals;
    private final LinkedList<Sensor> activeSensors = new LinkedList<>();
    public final GiListenerCrossbar<SensorEvent> sensorEventCrossbar = new GiListenerCrossbar<>();

    public GmAndroidSensorSetupService(OutOfSessionRepeatableGlobals globals) {
        this.globals = globals;
    }

    @Override
    public boolean svcHasThread() {
        return false;
    }

    /**
     * Ensure a sensor is connected.
     * The Sensor instance must be from the globals.
     */
    public synchronized void ensureSensor(Sensor sensor) {
        if (!activeSensors.contains(sensor))
            activeSensors.add(sensor);
    }

    @Override
    public synchronized void svcStart(boolean onThread) {
        for (Sensor sensor : activeSensors)
            globals.sensorManager.registerListener(this, sensor, 10000);
    }

    @Override
    public synchronized void svcEnd(boolean onThread) {
        globals.sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        sensorEventCrossbar.receive(sensorEvent);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void svcLoop() {

    }
}
