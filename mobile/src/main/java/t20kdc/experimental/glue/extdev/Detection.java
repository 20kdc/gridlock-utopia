package t20kdc.experimental.glue.extdev;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;

import java.util.LinkedList;

public class Detection {
    public static ExternalSensor[] scan(OutOfSessionRepeatableGlobals globals) {
        LinkedList<ExternalSensor> ext = new LinkedList<>();
        LinkedList<Integer> rotationVectorSensors = new LinkedList<>();
        LinkedList<Integer> linearAccelerationSensors = new LinkedList<>();
        try {
            for (int index = 0; index < globals.sensors.length; index++) {
                int st = globals.sensors[index].getType();
                if (isRotationVectorSensor(st))
                    rotationVectorSensors.add(index);
                if (isLinearAccelerationSensor(st))
                    linearAccelerationSensors.add(index);
                ext.add(new AndroidExternalSensor(globals.ctx, globals.sensors[index], index));
            }
            for (Integer rotVec : rotationVectorSensors)
                for (Integer linac : linearAccelerationSensors)
                    ext.add(new IMSExternalSensor(globals.ctx, globals.sensors[rotVec], rotVec, globals.sensors[linac], linac));
        } catch (Exception e) {
            // whoops
            e.printStackTrace();
        }
        return ext.toArray(new ExternalSensor[0]);
    }

    @SuppressLint("InlinedApi")
    public static boolean isRotationVectorSensor(int sensorType) {
        // Safety: this is literally just checking a constant against other values.
        return (sensorType == Sensor.TYPE_ROTATION_VECTOR) || (sensorType == Sensor.TYPE_GAME_ROTATION_VECTOR) || (sensorType == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR);
    }

    @SuppressLint("InlinedApi")
    public static boolean isLinearAccelerationSensor(int sensorType) {
        // Safety: this is literally just checking a constant against other values.
        return sensorType == Sensor.TYPE_LINEAR_ACCELERATION;
    }
}
