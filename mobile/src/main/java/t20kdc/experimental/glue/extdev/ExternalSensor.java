package t20kdc.experimental.glue.extdev;

import android.app.PendingIntent;
import android.content.Context;
import t20kdc.experimental.glue.cfg.CalibrationVector;
import t20kdc.experimental.glue.svc.GiListener;
import t20kdc.experimental.glue.svc.GiSensor;
import t20kdc.experimental.glue.svc.GiServiceManager;

/**
 * General (non-special) external sensor
 */
public abstract class ExternalSensor {
    public final Context ctx;

    public ExternalSensor(Context ctx) {
        this.ctx = ctx;
    }

    public abstract String getUniqueId();
    public abstract String getDetectionReport();
    public abstract boolean hasPermission();
    public abstract void askForPermission(PendingIntent returner);
    public abstract void open(GiServiceManager svc, GiListener<GiSensor> target, CalibrationVector cVec);
}
