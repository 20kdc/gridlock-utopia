package t20kdc.experimental.glue.extdev;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

/**
 * Repeatably sets up Sensor, SensorManager, Context variables.
 */
public class OutOfSessionRepeatableGlobals {
    public final Context ctx;
    public final SensorManager sensorManager;
    public final Sensor[] sensors;

    public OutOfSessionRepeatableGlobals(Context ctx) {
        this.ctx = ctx;
        sensorManager = (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);
        sensors = sensorManager.getSensorList(Sensor.TYPE_ALL).toArray(new Sensor[0]);
    }
}
