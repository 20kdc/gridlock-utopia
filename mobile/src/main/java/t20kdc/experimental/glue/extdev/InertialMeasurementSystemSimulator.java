package t20kdc.experimental.glue.extdev;

/**
 * Not thread-safe, handle with care.
 */
public class InertialMeasurementSystemSimulator {
    // Output: Position in metres, relative to world
    public double posX, posY, posZ;
    // Middle: Linear speed in metres/second, relative to world
    public double speedX, speedY, speedZ;
    // Input: Linear acceleration in metres/second, relative to device
    public double linacX, linacY, linacZ;

    // Input: Rotation matrix
    public float[] rotationMatrix = new float[9];

    private double getGlobalLinacAxis(int a, int b, int c) {
        return (rotationMatrix[a] * linacX) + (rotationMatrix[b] * linacY) + (rotationMatrix[c] * linacZ);
    }

    public void run(double time) {
        // Linac global
        double linacGlobalX = getGlobalLinacAxis(0, 1, 2);
        double linacGlobalY = getGlobalLinacAxis(3, 4, 5);
        double linacGlobalZ = getGlobalLinacAxis(6, 7, 8);
        // Speed
        speedX += linacGlobalX * time;
        speedY += linacGlobalY * time;
        speedZ += linacGlobalZ * time;
        // Decay to prevent accumulating drift
        speedX *= 0.999;
        speedY *= 0.999;
        speedZ *= 0.999;
        // Position
        posX += speedX * time;
        posY += speedY * time;
        posZ += speedZ * time;
        // Decay to prevent accumulating drift
        posX *= 0.9999;
        posY *= 0.9999;
        posZ *= 0.9999;
    }
}
