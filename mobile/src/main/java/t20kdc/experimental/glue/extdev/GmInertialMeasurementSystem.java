package t20kdc.experimental.glue.extdev;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

import t20kdc.experimental.glue.cfg.CalibrationVector;
import t20kdc.experimental.glue.svc.GiListener;
import t20kdc.experimental.glue.svc.GiSensor;
import t20kdc.experimental.glue.svc.GiService;

public class GmInertialMeasurementSystem implements GiService, GiListener<SensorEvent>, GiSensor {
    public final String methodName;
    public final Sensor rotationVectorSensor;
    public final Sensor linearAccelerationSensor;
    public final float[] data = {0, 0, 0};
    private final GiListener<GiSensor> target;
    private final CalibrationVector cVec;
    // sensor receiver does this
    private float upcomingLinacZeroX, upcomingLinacZeroY, upcomingLinacZeroZ;
    // on simulator thread
    private long lastSimulatorRunNanos = 0;
    private final InertialMeasurementSystemSimulator simulator = new InertialMeasurementSystemSimulator();
    // anyway...
    public GmInertialMeasurementSystem(GmAndroidSensorSetupService as, String methodName, Sensor rotationVectorSensor, Sensor linearAccelerationSensor, GiListener<GiSensor> target, CalibrationVector cVec) {
        this.methodName = methodName;
        this.rotationVectorSensor = rotationVectorSensor;
        this.linearAccelerationSensor = linearAccelerationSensor;
        this.target = target;
        this.cVec = cVec;
        as.ensureSensor(rotationVectorSensor);
        as.ensureSensor(linearAccelerationSensor);
        as.sensorEventCrossbar.add(this);
    }

    @Override
    public synchronized void receive(SensorEvent value) {
        if (value.sensor == this.rotationVectorSensor) {
            SensorManager.getRotationMatrixFromVector(simulator.rotationMatrix, value.values);
        } else if (value.sensor == this.linearAccelerationSensor) {
            // actual useful stuff
            simulator.linacX = value.values[0] - cVec.cvIMSZeroX;
            simulator.linacY = value.values[1] - cVec.cvIMSZeroY;
            simulator.linacZ = value.values[2] - cVec.cvIMSZeroZ;
            // and zeroing
            upcomingLinacZeroX = (value.values[0] + (upcomingLinacZeroX * 15)) / 16;
            upcomingLinacZeroY = (value.values[1] + (upcomingLinacZeroY * 15)) / 16;
            upcomingLinacZeroZ = (value.values[2] + (upcomingLinacZeroZ * 15)) / 16;
        }
    }

    @Override
    public boolean svcHasThread() {
        return true;
    }

    @Override
    public void svcStart(boolean onThread) {
        target.receive(this);
    }

    @Override
    public void svcEnd(boolean onThread) {

    }

    @Override
    public void svcLoop() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            // nope
        }
        runSimulator();
    }

    private synchronized void runSimulator() {
        long thisSimulatorRunNanos = System.nanoTime();
        long diff = thisSimulatorRunNanos - lastSimulatorRunNanos;
        if (lastSimulatorRunNanos == 0)
            diff = 0;
        double diffInSeconds = diff / 1000000000d;
        simulator.run(diffInSeconds);
        lastSimulatorRunNanos = thisSimulatorRunNanos;
        data[0] = (float) simulator.posX;
        data[1] = (float) simulator.posY;
        data[2] = (float) simulator.posZ;
    }

    @Override
    public String getUniqueName() {
        return methodName;
    }

    @Override
    public float[] getLastValues() {
        return data;
    }

    public synchronized void zeroLinac() {
        cVec.cvIMSZeroX = upcomingLinacZeroX;
        cVec.cvIMSZeroY = upcomingLinacZeroY;
        cVec.cvIMSZeroZ = upcomingLinacZeroZ;
        simulator.posX = 0;
        simulator.posY = 0;
        simulator.posZ = 0;
        simulator.speedX = 0;
        simulator.speedY = 0;
        simulator.speedZ = 0;
        simulator.linacX = 0;
        simulator.linacY = 0;
        simulator.linacZ = 0;
    }

    public synchronized void zeroPos() {
        simulator.posX = 0;
        simulator.posY = 0;
        simulator.posZ = 0;
        simulator.speedX = 0;
        simulator.speedY = 0;
        simulator.speedZ = 0;
    }
}
