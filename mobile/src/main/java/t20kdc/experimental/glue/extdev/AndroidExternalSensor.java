package t20kdc.experimental.glue.extdev;

import android.app.PendingIntent;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import t20kdc.experimental.glue.cfg.CalibrationVector;
import t20kdc.experimental.glue.svc.GiListener;
import t20kdc.experimental.glue.svc.GiSensor;
import t20kdc.experimental.glue.svc.GiServiceManager;

public class AndroidExternalSensor extends ExternalSensor {
    private final int index;
    private final Sensor sensor;
    public AndroidExternalSensor(Context ctx, Sensor s, int index) {
        super(ctx);
        this.index = index;
        sensor = s;
    }

    @Override
    public String getUniqueId() {
        return "android." + index;
    }

    @Override
    public String getDetectionReport() {
        return sensor.getVendor() + " " + sensor.getName();
    }

    @Override
    public boolean hasPermission() {
        return true;
    }

    @Override
    public void askForPermission(PendingIntent returner) {

    }

    @Override
    public void open(GiServiceManager svc, GiListener<GiSensor> target, CalibrationVector cVec) {
        GmAndroidSensorSetupService as = svc.getService(GmAndroidSensorSetupService.class);
        svc.add(new GmAndroidSensorService(as, index, sensor, target));
    }
}
