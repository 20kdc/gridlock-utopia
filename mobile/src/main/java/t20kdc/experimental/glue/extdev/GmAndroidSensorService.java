package t20kdc.experimental.glue.extdev;

import android.annotation.SuppressLint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;

import t20kdc.experimental.glue.svc.GiListener;
import t20kdc.experimental.glue.svc.GiSensor;
import t20kdc.experimental.glue.svc.GiService;

import java.util.concurrent.atomic.AtomicReference;

public class GmAndroidSensorService implements GiService, GiListener<SensorEvent>, GiSensor {
    private final String uniqueName;
    private final String uniqueNameMatrix;
    private final AtomicReference<float[]> ref = new AtomicReference<>(new float[0]);
    private final Sensor sensor;
    private final GiListener<GiSensor> l;
    private final boolean hasRotationVectorMatrixChannel;
    private final float[] rotationVectorMatrix = new float[9];
    public GmAndroidSensorService(GmAndroidSensorSetupService sm, int index, Sensor s, GiListener<GiSensor> ls) {
        l = ls;
        sensor = s;
        sm.ensureSensor(sensor);
        sm.sensorEventCrossbar.add(this);
        int sensorType = sensor.getType();
        String stringType;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            stringType = s.getStringType();
        } else {
            stringType = null;
            // Polyfill: emulate getStringType for SDK 9 (read: important) sensor types
            if (sensorType == Sensor.TYPE_ACCELEROMETER)
                stringType = "android.sensor.accelerometer";
            else if (sensorType == Sensor.TYPE_MAGNETIC_FIELD)
                stringType = "android.sensor.magnetic_field";
            else if (sensorType == Sensor.TYPE_ORIENTATION)
                stringType = "android.sensor.orientation";
            else if (sensorType == Sensor.TYPE_GYROSCOPE)
                stringType = "android.sensor.gyroscope";
            else if (sensorType == Sensor.TYPE_LIGHT)
                stringType = "android.sensor.light";
            else if (sensorType == Sensor.TYPE_PRESSURE)
                stringType = "android.sensor.pressure";
            else if (sensorType == Sensor.TYPE_TEMPERATURE)
                stringType = "android.sensor.temperature";
            else if (sensorType == Sensor.TYPE_PROXIMITY)
                stringType = "android.sensor.proximity";
            else if (sensorType == Sensor.TYPE_GRAVITY)
                stringType = "android.sensor.gravity";
            else if (sensorType == Sensor.TYPE_LINEAR_ACCELERATION)
                stringType = "android.sensor.linear_acceleration";
            else if (sensorType == Sensor.TYPE_ROTATION_VECTOR)
                stringType = "android.sensor.rotation_vector";
            else {
                stringType = "android.unknown." + sensorType;
            }
        }
        uniqueName = stringType + "/" + index;
        uniqueNameMatrix = stringType + "/matrix/" + index;
        hasRotationVectorMatrixChannel = Detection.isRotationVectorSensor(sensorType);
    }

    @Override
    public boolean svcHasThread() {
        return false;
    }

    @Override
    public void svcStart(boolean onThread) {
        l.receive(this);
        if (hasRotationVectorMatrixChannel) {
            // automatically also broadcast a second channel for matrix info
            l.receive(new GiSensor() {
                @Override
                public String getUniqueName() {
                    return uniqueNameMatrix;
                }

                @Override
                public float[] getLastValues() {
                    return rotationVectorMatrix;
                }
            });
        }
    }

    @Override
    public void svcLoop() {

    }

    @Override
    public void svcEnd(boolean onThread) {
    }

    @Override
    public String getUniqueName() {
        return uniqueName;
    }

    @Override
    public float[] getLastValues() {
        return ref.get();
    }

    @Override
    public void receive(SensorEvent value) {
        if (value.sensor != sensor)
            return;
        ref.set(value.values);
        if (hasRotationVectorMatrixChannel)
            SensorManager.getRotationMatrixFromVector(rotationVectorMatrix, value.values);
    }
}
