package t20kdc.experimental.glue;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import t20kdc.experimental.glue.cfg.GlueConfig;
import t20kdc.experimental.glue.svc.GiSensor;
import t20kdc.experimental.glue.svc.GiService;
import t20kdc.experimental.glue.svc.GiServiceActivity;

public class HandActivity extends GiServiceActivity {
    private final float[] hand_pos = {0, 0, 0, 0};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_the_hand);

        GlueConfig cfg = new GlueConfig(this);
        cfg.load();

        GlueSession oldCore = new GlueSession(this, cfg);
        oldCore.addBasicIO(glueServices);
        oldCore.addExternalSensors(glueServices);
        attachHandListener(findViewById(R.id.the_hand_xy), (x, y) -> {
            hand_pos[0] = x;
            hand_pos[1] = y;
        });
        attachHandListener(findViewById(R.id.the_hand_y), (x, y) -> {
            hand_pos[2] = x;
            hand_pos[3] = y;
        });
        glueServices.add(new GiService() {
            @Override
            public boolean svcHasThread() {
                return false;
            }

            @Override
            public void svcStart(boolean onThread) {
                oldCore.beacon.receive(new GiSensor() {
                    @Override
                    public String getUniqueName() {
                        return "the_hand";
                    }

                    @Override
                    public float[] getLastValues() {
                        return hand_pos;
                    }
                });
            }

            @Override
            public void svcEnd(boolean onThread) {

            }

            @Override
            public void svcLoop() {

            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void attachHandListener(View viewById, HandListener hl) {
        viewById.setOnTouchListener((view, motionEvent) -> {
            int am = motionEvent.getActionMasked();
            if (am == MotionEvent.ACTION_DOWN || am == MotionEvent.ACTION_UP || am == MotionEvent.ACTION_MOVE) {
                // firstly, convert coordinates to centre relative space
                float wd2 = viewById.getWidth() / 2f;
                float hd2 = viewById.getHeight() / 2f;
                float x = motionEvent.getX() - wd2;
                float y = motionEvent.getY() - hd2;
                float minOfWH = Math.min(wd2, hd2);
                x /= minOfWH;
                y /= minOfWH;
                hl.receive(x, y);
            }
            return true;
        });
    }
    interface HandListener {
        void receive(float x, float y);
    }
}
