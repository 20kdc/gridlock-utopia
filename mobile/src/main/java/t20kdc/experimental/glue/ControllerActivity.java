package t20kdc.experimental.glue;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;

import t20kdc.experimental.glue.cfg.GlueConfig;
import t20kdc.experimental.glue.extdev.GmInertialMeasurementSystem;
import t20kdc.experimental.glue.svc.GiListener;
import t20kdc.experimental.glue.svc.GiSensor;
import t20kdc.experimental.glue.svc.GiService;
import t20kdc.experimental.glue.svc.GiServiceActivity;

public class ControllerActivity extends GiServiceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_controller);

        GlueConfig cfg = new GlueConfig(this);
        cfg.load();

        GlueSession oldCore = new GlueSession(this, cfg);
        oldCore.addBasicIO(glueServices);
        oldCore.addExternalSensors(glueServices);
        findViewById(R.id.ctrlZeroLinac).setOnClickListener((view) -> {
            GmInertialMeasurementSystem ims = glueServices.getService(GmInertialMeasurementSystem.class);
            if (ims == null)
                return;
            ims.zeroLinac();
            cfg.save();
        });
        findViewById(R.id.ctrlZeroPos).setOnClickListener((view) -> {
            GmInertialMeasurementSystem ims = glueServices.getService(GmInertialMeasurementSystem.class);
            if (ims == null)
                return;
            ims.zeroPos();
        });
        int[] ids = {
                R.id.ctrl1,
                R.id.ctrl2,
                R.id.ctrl3,
                R.id.ctrl4,
                R.id.ctrl5,
                R.id.ctrl6,
        };
        int idx = 1;
        for (int id : ids) {
            final String ctrl = "button." + idx;
            idx++;
            final TouchSensor ts = new TouchSensor(ctrl, oldCore.beacon);
            glueServices.add(ts);
            // This is intentional!!! This routes through the duration/etc of the press live.
            findViewById(id).setOnTouchListener(new View.OnTouchListener() {
                @SuppressLint("ClickableViewAccessibility")
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    int actionMasked = motionEvent.getActionMasked();
                    // this used to do more, but that turned out to be buggy if the activity is switched out
                    if (actionMasked == MotionEvent.ACTION_DOWN || actionMasked == MotionEvent.ACTION_POINTER_DOWN)
                        ts.status = true;
                    if (actionMasked == MotionEvent.ACTION_UP || actionMasked == MotionEvent.ACTION_POINTER_UP)
                        ts.status = false;
                    return false;
                }
            });
        }
    }

    private static class TouchSensor implements GiSensor, GiService {
        private final String ctrl;
        public volatile boolean status;
        private final float[] dataOn = {1};
        private final float[] dataOff = {0};
        private final GiListener<GiSensor> sensorReceiver;

        public TouchSensor(String ctrl, GiListener<GiSensor> sensorReceiver) {
            this.ctrl = ctrl;
            this.sensorReceiver = sensorReceiver;
        }

        @Override
        public String getUniqueName() {
            return ctrl;
        }

        @Override
        public float[] getLastValues() {
            return status ? dataOn : dataOff;
        }

        @Override
        public boolean svcHasThread() {
            return false;
        }

        @Override
        public void svcStart(boolean onThread) {
            sensorReceiver.receive(this);
        }

        @Override
        public void svcEnd(boolean onThread) {

        }

        @Override
        public void svcLoop() {

        }
    }
}
