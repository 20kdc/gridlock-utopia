package t20kdc.experimental.glue.cfg;

import android.content.SharedPreferences;

public class CalibrationVector {
    public volatile float cvIMSZeroX;
    public volatile float cvIMSZeroY;
    public volatile float cvIMSZeroZ;

    public CalibrationVector() {

    }

    public void load(SharedPreferences sp) {
        cvIMSZeroX = sp.getFloat("cvIMSZeroX", 0);
        cvIMSZeroY = sp.getFloat("cvIMSZeroY", 0);
        cvIMSZeroZ = sp.getFloat("cvIMSZeroZ", 0);
    }

    public void save(SharedPreferences.Editor ed) {
        ed.putFloat("cvIMSZeroX", cvIMSZeroX);
        ed.putFloat("cvIMSZeroY", cvIMSZeroY);
        ed.putFloat("cvIMSZeroZ", cvIMSZeroZ);
    }
}
