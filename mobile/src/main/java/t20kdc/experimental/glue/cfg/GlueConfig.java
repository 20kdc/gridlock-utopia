package t20kdc.experimental.glue.cfg;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;

public class GlueConfig {
    public String name = "gridlock";
    public String targetAddress = "255.255.255.255";
    // would be nice to fold this into targetAddress or something
    public int targetPort = 9000;
    public final HashSet<String> externalSensorEnable = new HashSet<>();
    public final CalibrationVector cVec = new CalibrationVector();

    private final SharedPreferences sp;

    public GlueConfig(Context ctx) {
        sp = ctx.getSharedPreferences("GlueConfig", Context.MODE_PRIVATE);
    }

    public void load() {
        name = sp.getString("name", "gridlock");
        targetAddress = sp.getString("targetAddress", "255.255.255.255");
        targetPort = sp.getInt("targetPort", 9000);
        externalSensorEnable.clear();
        for (String srs : sp.getAll().keySet())
            if (srs.startsWith("externalSensorEnable."))
                externalSensorEnable.add(srs.substring(21));
        cVec.load(sp);
    }

    public void save() {
        SharedPreferences.Editor ed = sp.edit();
        ed.clear();
        ed.putString("name", name);
        ed.putString("targetAddress", targetAddress);
        ed.putInt("targetPort", targetPort);
        for (String ese : externalSensorEnable)
            ed.putBoolean("externalSensorEnable." + ese, true);
        cVec.save(ed);
        ed.apply();
    }
}
