package t20kdc.experimental.glue;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import t20kdc.experimental.glue.cfg.GlueConfig;
import t20kdc.experimental.glue.svc.GiServiceManager;

public class PivotService extends Service {
    public GiServiceManager glueServices;

    public PivotService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String intentAction = intent.getAction();
            if (intentAction != null && intentAction.equals("STOP")) {
                stopSelf();
                return START_NOT_STICKY;
            }
        }
        // oops
        if (glueServices != null) {
            Log.println(Log.ERROR, "GridlockUtopia", "Pivot already has glueServices.");
            return START_NOT_STICKY;
        }
        // make background notif
        Intent stopMe = new Intent(this, PivotService.class);
        stopMe.setAction("STOP");
        String text = getString(R.string.pivot_service_description);
        Notification notif = new Notification(R.drawable.ic_launcher, text, 1000);
        notif.flags |= Notification.FLAG_AUTO_CANCEL;
        notif.flags |= Notification.FLAG_FOREGROUND_SERVICE;
        notif.deleteIntent = notif.contentIntent = PendingIntent.getService(this, 0, stopMe, 0);
        startForeground(R.string.pivot_service_description, notif);
        // alright, now we actually do the thing
        glueServices = new GiServiceManager(new NotificationErrorHandler(this));

        // we have to use the config from this point, sadly
        final GlueConfig config = new GlueConfig(this);
        config.load();

        final GlueSession oldCore = new GlueSession(this, config);
        oldCore.addBasicIO(glueServices);
        oldCore.addExternalSensors(glueServices);

        glueServices.setStatus(true);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        glueServices.setStatus(false);
        glueServices = null;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
