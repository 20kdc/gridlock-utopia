package t20kdc.experimental.glue;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import t20kdc.experimental.glue.cfg.GlueConfig;
import t20kdc.experimental.glue.extdev.Detection;
import t20kdc.experimental.glue.extdev.ExternalSensor;
import t20kdc.experimental.glue.extdev.OutOfSessionRepeatableGlobals;

import java.util.LinkedList;

public class MainActivity extends Activity {

    private GlueConfig mirror;
    private final LinkedList<Runnable> saveUiIntoConfig = new LinkedList<>();
    private final LinkedList<Runnable> loadConfigIntoUi = new LinkedList<>();

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        mirror = new GlueConfig(this);

        loadConfigIntoUi.add(() -> {
            ((TextView) findViewById(R.id.editText)).setText(mirror.name);
            ((TextView) findViewById(R.id.editText2)).setText(mirror.targetAddress);
            ((TextView) findViewById(R.id.editTextPort)).setText(Integer.toString(mirror.targetPort));
        });
        saveUiIntoConfig.add(() -> {
            mirror.name = ((TextView) findViewById(R.id.editText)).getText().toString();
            mirror.targetAddress = ((TextView) findViewById(R.id.editText2)).getText().toString();
            try {
                mirror.targetPort = Integer.parseInt(((TextView) findViewById(R.id.editTextPort)).getText().toString());
            } catch (Exception ex) {
                // do nothing
            }
        });

        findViewById(R.id.mainBeginButton).setOnClickListener(v -> {
            stopService(new Intent(MainActivity.this, PivotService.class));
            leaving();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(new Intent(MainActivity.this, PivotService.class));
            } else {
                startService(new Intent(MainActivity.this, PivotService.class));
            }
            finish();
        });
        findViewById(R.id.mainControllerButton).setOnClickListener(v -> {
            stopService(new Intent(MainActivity.this, PivotService.class));
            leaving();
            startActivity(new Intent(MainActivity.this, ControllerActivity.class));
        });
        findViewById(R.id.mainHandButton).setOnClickListener(v -> {
            stopService(new Intent(MainActivity.this, PivotService.class));
            leaving();
            startActivity(new Intent(MainActivity.this, HandActivity.class));
        });
        findViewById(R.id.mainLicensingButton).setOnClickListener(v -> {
            leaving();
            startActivity(new Intent(MainActivity.this, LicensingActivity.class));
        });

        LinearLayout ll = findViewById(R.id.deviceAttachmentLayout);
        for (final ExternalSensor es : Detection.scan(new OutOfSessionRepeatableGlobals(this))) {
            if (!es.hasPermission()) {
                TextView information = new TextView(this);
                information.setText(es.getDetectionReport());
                ll.addView(information);
                information = new TextView(this);
                information.setText(R.string.sp_a2_wakeup_sensor_permissions);
                ll.addView(information);
                Button btn = new Button(this);
                btn.setText(R.string.sp_a2_wakeup_sensor_permissions_button);
                btn.setOnClickListener(v -> {
                    leaving();
                    es.askForPermission(PendingIntent.getActivity(MainActivity.this, 42, new Intent(MainActivity.this, MainActivity.class), 0));
                });
                ll.addView(btn);
            } else {
                final CheckBox enable = new CheckBox(this);
                enable.setText(es.getDetectionReport());
                ll.addView(enable);
                loadConfigIntoUi.add(() -> enable.setChecked(mirror.externalSensorEnable.contains(es.getUniqueId())));
                saveUiIntoConfig.add(() -> {
                    if (enable.isChecked()) {
                        mirror.externalSensorEnable.add(es.getUniqueId());
                    } else {
                        mirror.externalSensorEnable.remove(es.getUniqueId());
                    }
                });
            }
        }

        mirror.load();
        for (Runnable r : loadConfigIntoUi)
            r.run();
    }

    private void leaving() {
        for (Runnable r : saveUiIntoConfig)
            r.run();
        mirror.save();
    }

    @Override
    protected void onStop() {
        super.onStop();
        leaving();
    }
}
