package t20kdc.experimental.glue;

import android.app.Activity;
import android.widget.Toast;
import t20kdc.experimental.glue.svc.GiListener;

public class GlueErrorHandler implements GiListener<Exception> {
    public final Activity ctx;
    public GlueErrorHandler(Activity ct) {
        ctx = ct;
    }
    @Override
    public void receive(Exception value) {
        ctx.runOnUiThread(() -> {
            Toast t = Toast.makeText(ctx, R.string.sp_a3_error, Toast.LENGTH_LONG);
            t.show();
        });
        value.printStackTrace();
    }
}
