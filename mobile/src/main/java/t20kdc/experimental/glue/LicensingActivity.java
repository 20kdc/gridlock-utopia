package t20kdc.experimental.glue;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;

public class LicensingActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_licensing);
        ((WebView) findViewById(R.id.galaxy)).loadUrl("file:///android_asset/guide.html");
    }
}
