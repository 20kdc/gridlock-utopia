package t20kdc.experimental.glue;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import t20kdc.experimental.glue.svc.GiListener;

public class NotificationErrorHandler implements GiListener<Exception> {
    public final Context ctx;
    public NotificationErrorHandler(Context ctx) {
        this.ctx = ctx;
    }
    @Override
    public void receive(Exception value) {
        value.printStackTrace();
        Notification notif = new Notification(R.drawable.ic_launcher, "Exception in GridlockUtopia service", 0);
        NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(R.string.pivot_error_notification, notif);
    }
}
