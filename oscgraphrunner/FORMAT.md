# OSCGraphRunner config format

* `vm_generation`: 'VM generation' (config version during a runtime session). Set automatically by the server on config upload.
* `control_addr`: Control server socket address
* `control_password`: Control server password
* `tick_time`: Tick time in seconds
* `floats_init`: All VM float data
* `threads`: Array of `Thread`.
* `buses`: Array of `Program`. Each program represents the code to run when a message comes in on that bus.
* `tick`: Tick `Program`

Also beware all the Additional Stuff added by OSCGraphEditor.

## Program

Programs are arrays of program steps.

Program steps are arrays where the first value indicates the step type.

The state of the machine consists of:

* Float data, initially `floats_init`
* ALU0 data
* The current received or created OSC message (not packet, message)
* The bus queues

Before and after each tick, the bus programs are run for everything on the bus queues until they are all empty.

For info on the ISA please see `program.rs` for now.

To simplify things, there are a very small number of "instruction formats":

* `Nop`: `[op]`: `nop`
* `If`: `[op, value, yes, no]`: `if_=0`, `if_!=0`, `if_>0`, `if_>=0`, `if_<0`, `if_<=0`
* `ALU0`: `[op, dest]`: `time_mono`, `time_epoch`
* `ALU1`: `[op, dest, a]`: `copy`, `sqrt`, `sin`, `cos`, `tan`, `round`, `floor`, `ceil`, `signum`, `fract`, `abs`, `recip`, `trunc`, `asin`, `acos`, `atan`, `sinh`, `cosh`, `tanh`, `asinh`, `acosh`, `atanh`, `to_radians`, `to_degrees`, `invert`, `negate`
* `ALU2`: `[op, dest, a, b]`: `add`, `sub`, `div`, `mul`, `pow`, `max`, `min`, `atan2`
* `CreateOSCMessage`: `[op, schematic]`: `osc_create`
* `ForwardOSCMessage`: `[op, bus]`: `osc_forward`
* `DecodeOSCMessage`: `[op, schematic, ok, fail]`: `osc_decode`
* `SendOSCMessage`: `[op, address]`: `osc_send`

## ProgramOSCSchematic

Array of 3: `[method, args_start, args_len]`

## Thread

Threads are arrays where the first value indicates the thread type.

This is similar to steps so this will be shown similarly

* `OscInput`: `[op, address, target_bus]`: `osc_input`
