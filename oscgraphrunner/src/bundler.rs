use std::net::{Ipv4Addr, Ipv6Addr, SocketAddr, UdpSocket};

use rosc::{OscBundle, OscMessage, OscPacket, OscTime};

pub struct OscBundler {
    state: OscBundlerState,
    /// OSC output socket (v4)
    osc_output_socket_v4: UdpSocket,
    /// OSC output socket (v6)
    osc_output_socket_v6: UdpSocket
}
enum OscBundlerState {
    Inactive,
    Active(SocketAddr, Vec<OscMessage>)
}

impl OscBundler {
    pub fn new() -> Self {
        Self {
            state: OscBundlerState::Inactive,
            osc_output_socket_v4: UdpSocket::bind(SocketAddr::new(Ipv4Addr::UNSPECIFIED.into(), 0)).unwrap(),
            osc_output_socket_v6: UdpSocket::bind(SocketAddr::new(Ipv6Addr::UNSPECIFIED.into(), 0)).unwrap()
        }
    }
    /// Flushes.
    pub fn flush(&mut self) {
        if let OscBundlerState::Active(addr, mv) = &mut self.state {
            let socket = match addr {
                SocketAddr::V4(_) => &self.osc_output_socket_v4,
                SocketAddr::V6(_) => &self.osc_output_socket_v6,
            };
            let content = mv.drain(..).map(|v| OscPacket::Message(v)).collect();
            // rosc's implementation of OscTime will break in 2038.
            // In theory this is an OSC protocol error but what exactly do you expect me to do about it?
            // As it is, fake this way for now and figure it out later if that causes bugs
            let timetag = OscTime { seconds: 0, fractional: 0 };
            let res = rosc::encoder::encode(&OscPacket::Bundle(OscBundle { timetag, content })).unwrap();
            _ = socket.send_to(&res, *addr);
        }
        self.state = OscBundlerState::Inactive;
    }
    /// Pushes a new message into the bundler, flushing if necessary.
    pub fn push(&mut self, target: SocketAddr, msg: OscMessage) {
        if let OscBundlerState::Active(addr, mq) = &mut self.state {
            if (*addr).eq(&target) {
                mq.push(msg);
            } else {
                self.flush();
                self.state = OscBundlerState::Active(target, vec![msg]);
            }
        } else {
            self.state = OscBundlerState::Active(target, vec![msg]);
        }
    }
}
