use std::{net::{SocketAddr, UdpSocket}, str::FromStr, sync::{atomic::{AtomicBool, Ordering}, Arc}, thread::JoinHandle, time::Duration};

use json::JsonValue;

use crate::hub::{Hub, HubMessage};

/// Creates a VM background/input thread.
pub fn create_thread(src: &JsonValue, vmgen: u64, hub: Arc<Hub>, this_vm_is_shutting_down: Arc<AtomicBool>) -> Option<JoinHandle<()>> {
    match src {
        JsonValue::Array(arr) => {
            let op = arr[0].as_str().unwrap();
            if op.eq("osc_input") {
                // ["osc_input", addr, bus]
                let iix = arr[1].as_str().unwrap();
                let k = arr[2].as_usize().unwrap();
                if let Ok(res) = SocketAddr::from_str(iix) {
                    let socket = UdpSocket::bind(res);
                    match socket {
                        Err(err) => {
                            eprintln!("could not bind OSC input: {}", err);
                            None
                        },
                        Ok(socket) => {
                            // alright, kick it!
                            Some(std::thread::spawn(move || {
                                socket.set_read_timeout(Some(Duration::from_millis(50 as u64))).unwrap();
                                while !this_vm_is_shutting_down.load(Ordering::SeqCst) {
                                    let mut msg: [u8; 65536] = [0; 65536];
                                    match socket.recv(&mut msg) {
                                        Err(_) => {},
                                        Ok(len) => {
                                            let msg_truncated = &msg[0..len];
                                            if let Ok(res) = rosc::decoder::decode_udp(msg_truncated) {
                                                _ = hub.nexus_input.send(HubMessage::OscPacket(vmgen, k, res.1));
                                            }
                                        }
                                    }
                                }
                            }))
                        }
                    }
                } else {
                    eprintln!("could not parse OSC input: {}", iix);
                    None
                }
            } else {
                eprintln!("unknown thread type: {}", op);
                None
            }
        },
        _ => {
            // :(
            None
        }
    }
}
