use std::{ops::Add, path::PathBuf, sync::{atomic::{AtomicBool, AtomicU64, Ordering}, Arc, Mutex}, thread::JoinHandle, time::{Duration, Instant}};

use json::JsonValue;
use rosc::{OscMessage, OscPacket};

use crate::program::{self};
use crate::thread::{self};

/// Messages sent to the hub thread.
pub enum HubMessage {
    /// Indicates a config update has happened and the VM should be updated.
    UpdateConfig(Arc<JsonValue>),
    /// (vmgen, queue, packet) OSC packet from some thread or another
    OscPacket(u64, usize, OscPacket),
    /// (vmgen, contents) Modify store floats, useful for non-OSC inputs
    FloatPoke(u64, Vec<(usize, f64)>)
}

/// The Hub. Immutable struct shared between control/instance concerns.
pub struct Hub {
    /// Current configuration.
    /// Only set from hub thread!
    config: Mutex<Arc<JsonValue>>,
    /// Password
    pub password: String,
    /// Path to configuration file.
    config_path: PathBuf,
    /// Nexus input
    pub nexus_input: std::sync::mpsc::Sender<HubMessage>,
    /// Copy of the program store.
    pub store: Mutex<program::ProgramStore>,
    /// VM generation allocator
    pub vmgen_allocator: AtomicU64
}

impl Hub {
    pub fn new(config_path: PathBuf) -> std::io::Result<(Arc<Hub>, std::sync::mpsc::Receiver<HubMessage>)> {
        let config_text = std::fs::read_to_string(&config_path)?;
        let mut config_json = json::parse(&config_text).map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidData, e))?;
        _ = config_json.insert("vm_generation", json::value!(0));
        let password = config_json["control_password"].as_str().expect("config_password").to_string();
        let (nexus_input, nexus_output) = std::sync::mpsc::channel();
        let hub = Arc::new(Hub {
            config: Mutex::new(Arc::new(config_json)),
            password,
            config_path,
            nexus_input,
            store: Mutex::new(program::ProgramStore {
                generation: u64::max_value(),
                floats: vec![]
            }),
            vmgen_allocator: AtomicU64::new(1)
        });
        Ok((hub, nexus_output))
    }
    pub fn config(&self) -> Arc<JsonValue> {
        self.config.lock().unwrap().clone()
    }
    pub fn set_config(&self, mut cfg: JsonValue) -> u64 {
        let gen = self.vmgen_allocator.fetch_add(1, Ordering::SeqCst);
        _ = cfg.insert("vm_generation", json::value!(gen));
        _ = std::fs::write(&self.config_path, cfg.pretty(4));
        _ = self.nexus_input.send(HubMessage::UpdateConfig(Arc::new(cfg)));
        gen
    }
}

/// Hub thread setup.
pub struct HubThread {
    pub hub: Arc<Hub>,
    pub nexus_output: std::sync::mpsc::Receiver<HubMessage>,
    vm: VM,
    last_tick_time: Instant,
    next_tick_time: Instant
}

impl HubThread {
    pub fn start(hub: Arc<Hub>, nexus_output: std::sync::mpsc::Receiver<HubMessage>) {
        let cfg = &hub.config();
        let vm = VM::new(hub.clone(), cfg);
        std::thread::spawn(move || {
            let mut me = HubThread {
                hub,
                nexus_output,
                vm,
                last_tick_time: Instant::now(),
                next_tick_time: Instant::now()
            };
            me.run();
        });
    }
    fn run(&mut self) {
        loop {
            let duration_since = self.next_tick_time.duration_since(Instant::now());
            if duration_since.is_zero() {
                self.vm.tick();
                self.last_tick_time = self.next_tick_time;
                self.next_tick_time = self.last_tick_time.add(self.vm.tick_time);
            } else {
                match self.nexus_output.recv_timeout(duration_since) {
                    Ok(msg) => {
                        match msg {
                            HubMessage::UpdateConfig(config) => {
                                *self.hub.config.lock().unwrap() = config;
                                self.update_vm();
                            },
                            HubMessage::OscPacket(generation, idx, pkt) => {
                                if self.vm.env.store.generation == generation {
                                    self.vm.osc_packet(idx, &pkt);
                                }
                            }
                            HubMessage::FloatPoke(generation, vec) => {
                                if self.vm.env.store.generation == generation {
                                    for v in vec {
                                        self.vm.env.store.floats[v.0] = v.1;
                                    }
                                }
                            }
                        }
                    },
                    _ => {
                        // nothing to do
                    }
                }
            }
        }
    }
    fn update_vm(&mut self) {
        self.vm.shutdown();
        self.vm = VM::new(self.hub.clone(), &self.hub.config());
        // recalculate tick time
        self.next_tick_time = self.last_tick_time.add(self.vm.tick_time);
    }
}

/// VM.
struct VM {
    /// Tick time of the VM.
    tick_time: Duration,
    hub: Arc<Hub>,
    this_vm_is_shutting_down: Arc<AtomicBool>,
    threads_to_shut_down: Vec<JoinHandle<()>>,
    tick: program::VerifiedProgram,
    env: program::ProgramEnv,
    buses: Vec<program::VerifiedProgram>,
}

impl VM {
    /// Starts up the VM and supporting threads.
    /// This is also the documentation of the config format for the runner.
    fn new(hub: Arc<Hub>, config: &JsonValue) -> VM {
        let this_vm_is_shutting_down = Arc::new(AtomicBool::new(false));
        let buses: Vec<program::VerifiedProgram> = config["buses"].members().map(|v| {
            program::VerifiedProgram::verify(&program::decode_program(&v))
        }).collect();
        let mut env = program::ProgramEnv::new(&config, buses.len());
        env.update_time();
        let threads_to_shut_down = config["threads"].members().filter_map(|v| thread::create_thread(v, env.store.generation, hub.clone(), this_vm_is_shutting_down.clone())).collect();
        VM {
            hub,
            tick_time: Duration::from_secs_f64(config["tick_time"].as_f64().expect("tick_time")),
            this_vm_is_shutting_down,
            threads_to_shut_down,
            env,
            tick: program::VerifiedProgram::verify(&program::decode_program(&config["tick"])),
            buses,
        }
    }
    fn tick(&mut self) {
        self.env.update_time();
        self.process_queues();
        self.tick.run(&mut self.env);
        self.process_queues();
        self.env.osc_bundler.flush();
        // update debugging info
        self.hub.store.lock().unwrap().copy_from(&self.env.store);
    }
    /// Pops the next queue message to handle.
    /// Under normal circumstances this should sort the bus messages by queue, which is important to keep OSC bundling happy.
    fn pop_queue_message(&mut self) -> Option<(usize, OscMessage)> {
        for k in 0..self.env.osc_queues.len() {
            let queue = &mut self.env.osc_queues[k];
            if queue.is_empty() {
                continue;
            }
            return Some((k, queue.remove(0)));
        }
        None
    }
    fn process_queues(&mut self) {
        while let Some((k, msg)) = self.pop_queue_message() {
            // load OSC message, then run bus program to route it
            self.env.osc_message = msg;
            self.buses[k].run(&mut self.env);
        }
    }
    /// OSC message came in, handle it
    fn osc_message(&mut self, index: usize, msg: &OscMessage) {
        self.env.osc_queues[index].push(msg.clone());
    }
    fn osc_packet(&mut self, index: usize, pkt: &OscPacket) {
        match pkt {
            OscPacket::Message(msg) => self.osc_message(index, msg),
            OscPacket::Bundle(bnd) => {
                for v in &bnd.content {
                    self.osc_packet(index, v);
                }
            }
        }
    }

    fn shutdown(&mut self) {
        self.this_vm_is_shutting_down.store(true, Ordering::SeqCst);
        for v in self.threads_to_shut_down.drain(..) {
            _ = v.join();
        }
    }
}
