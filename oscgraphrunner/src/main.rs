use std::{net::{SocketAddr, TcpListener}, path::PathBuf, str::FromStr};

use crate::hub::{Hub, HubThread};
mod control;
mod hub;
mod program;
mod thread;
mod bundler;

fn main() {
    println!("OSCGraphRunner boot");
    let path: PathBuf = "config.json".into();
    if std::fs::metadata(&path).is_err_and(|e| e.kind() == std::io::ErrorKind::NotFound) {
        _ = std::fs::write(&path, "{\"control_addr\": \"127.0.0.1:8294\", \"control_password\": \"\", \"tick_time\": 0.016666666666667}");
    }
    let (hub, nex) = Hub::new(path).unwrap();
    HubThread::start(hub.clone(), nex);
    let cfg = hub.config();
    let tl = TcpListener::bind(SocketAddr::from_str(cfg["control_addr"].as_str().unwrap()).unwrap()).unwrap();
    control::server_thread(hub, tl);
}
