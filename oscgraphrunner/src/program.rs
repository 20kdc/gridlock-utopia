use std::{net::SocketAddr, str::FromStr, time::{Duration, Instant, SystemTime}};

use json::JsonValue;
use rosc::{OscMessage, OscType};

use crate::bundler::OscBundler;

/// OSC schematic. Used for OSC method IO.
#[derive(Clone)]
pub struct ProgramOSCSchematic {
    pub method: String,
    pub args_start: usize,
    pub args_len: usize,
}
impl ProgramOSCSchematic {
    pub fn decode(json: &JsonValue) -> ProgramOSCSchematic {
        ProgramOSCSchematic {
            method: json[0].as_str().unwrap().to_string(),
            args_start: json[1].as_usize().unwrap(),
            args_len: json[2].as_usize().unwrap()
        }
    }
    pub fn copy_from_store(&self, store: &ProgramStore) -> Vec<OscType> {
        let mut argvec = Vec::with_capacity(self.args_len);
        for i in 0..self.args_len {
            argvec.push(OscType::Float(store.floats[self.args_start + i] as f32));
        }
        argvec
    }
    pub fn copy_into_store(&self, store: &mut ProgramStore, data: &Vec<OscType>) -> bool {
        if data.len() != self.args_len {
            return false;
        }
        for (i, v) in data.iter().enumerate() {
            if let OscType::Float(v) = v {
                store.floats[self.args_start + i] = (*v) as f64;
            } else if let OscType::Int(v) = v {
                store.floats[self.args_start + i] = (*v) as f64;
            }
        }
        true
    }
}

/// Step in a program.
#[derive(Clone)]
pub enum ProgramStep {
    // do nothing
    Nop,
    /// if A(B) then C else D
    If(ProgramCondition, usize, Program, Program),
    /// A = B()
    ALU0(usize, ProgramALU0),
    /// A = B(C)
    ALU1(usize, ProgramALU1, usize),
    /// A = B(C, D)
    ALU2(usize, ProgramALU2, usize, usize),
    /// oscMessage = OSC(schematic A)
    CreateOSCMessage(ProgramOSCSchematic),
    /// send(A, oscMessage)
    ForwardOSCMessage(usize),
    /// if decodeOSC(A, oscMessage) then B else C
    DecodeOSCMessage(ProgramOSCSchematic, Program, Program),
    /// send(A, oscMessage)
    SendOSCMessage(SocketAddr),
}
/// Relation in a program.
#[derive(Clone, Copy)]
pub enum ProgramCondition {
    Zero,
    NotZero,
    AboveZero,
    AboveEqualZero,
    BelowZero,
    BelowEqualZero
}
impl ProgramCondition {
    pub fn by_str(name: &str) -> Option<ProgramCondition> {
        if name.eq("if_=0") {
            Some(ProgramCondition::Zero)
        } else if name.eq("if_!=0") {
            Some(ProgramCondition::NotZero)
        } else if name.eq("if_>0") {
            Some(ProgramCondition::AboveZero)
        } else if name.eq("if_>=0") {
            Some(ProgramCondition::AboveEqualZero)
        } else if name.eq("if_<0") {
            Some(ProgramCondition::BelowZero)
        } else if name.eq("if_<=0") {
            Some(ProgramCondition::BelowEqualZero)
        } else {
            None
        }
    }
    #[inline]
    pub fn eval(&self, v: f64) -> bool {
        match self {
            Self::Zero => v == 0.0,
            Self::NotZero => v != 0.0,
            Self::AboveZero => v > 0.0,
            Self::AboveEqualZero => v >= 0.0,
            Self::BelowZero => v < 0.0,
            Self::BelowEqualZero => v <= 0.0,
        }
    }
}
/// ALU0 op
#[derive(Clone, Copy)]
pub enum ProgramALU0 {
    TimeMono,
    TimeEpoch
}
impl ProgramALU0 {
    pub fn by_str(name: &str) -> Option<ProgramALU0> {
        if name.eq("time_mono") {
            Some(ProgramALU0::TimeMono)
        } else if name.eq("time_epoch") {
            Some(ProgramALU0::TimeEpoch)
        } else {
            None
        }
    }
    #[inline]
    pub fn eval(&self, env: &ProgramEnv) -> f64 {
        match self {
            Self::TimeMono => env.time_mono,
            Self::TimeEpoch => env.time_epoch,
        }
    }
}
/// ALU1 op
#[derive(Clone, Copy)]
pub enum ProgramALU1 {
    Copy,
    Sqrt,
    Sin,
    Cos,
    Tan,
    Round,
    Floor,
    Ceil,
    Signum,
    Fract,
    Abs,
    Recip,
    Trunc,
    Asin,
    Acos,
    Atan,
    Sinh,
    Cosh,
    Tanh,
    Asinh,
    Acosh,
    Atanh,
    ToRadians,
    ToDegrees,
    Invert,
    Negate
}
impl ProgramALU1 {
    pub fn by_str(name: &str) -> Option<ProgramALU1> {
        if name.eq("copy") {
            Some(ProgramALU1::Copy)
        } else if name.eq("sqrt") {
            Some(ProgramALU1::Sqrt)
        } else if name.eq("sin") {
            Some(ProgramALU1::Sin)
        } else if name.eq("cos") {
            Some(ProgramALU1::Cos)
        } else if name.eq("tan") {
            Some(ProgramALU1::Tan)
        } else if name.eq("round") {
            Some(ProgramALU1::Round)
        } else if name.eq("floor") {
            Some(ProgramALU1::Floor)
        } else if name.eq("ceil") {
            Some(ProgramALU1::Ceil)
        } else if name.eq("signum") {
            Some(ProgramALU1::Signum)
        } else if name.eq("fract") {
            Some(ProgramALU1::Fract)
        } else if name.eq("abs") {
            Some(ProgramALU1::Abs)
        } else if name.eq("recip") {
            Some(ProgramALU1::Recip)
        } else if name.eq("trunc") {
            Some(ProgramALU1::Trunc)
        } else if name.eq("asin") {
            Some(ProgramALU1::Asin)
        } else if name.eq("acos") {
            Some(ProgramALU1::Acos)
        } else if name.eq("atan") {
            Some(ProgramALU1::Atan)
        } else if name.eq("sinh") {
            Some(ProgramALU1::Sinh)
        } else if name.eq("cosh") {
            Some(ProgramALU1::Cosh)
        } else if name.eq("tanh") {
            Some(ProgramALU1::Tanh)
        } else if name.eq("asinh") {
            Some(ProgramALU1::Asinh)
        } else if name.eq("acosh") {
            Some(ProgramALU1::Acosh)
        } else if name.eq("atanh") {
            Some(ProgramALU1::Atanh)
        } else if name.eq("to_radians") {
            Some(ProgramALU1::ToRadians)
        } else if name.eq("to_degrees") {
            Some(ProgramALU1::ToDegrees)
        } else if name.eq("invert") {
            Some(ProgramALU1::Invert)
        } else if name.eq("negate") {
            Some(ProgramALU1::Negate)
        } else {
            None
        }
    }
    #[inline]
    pub fn eval(&self, v: f64) -> f64 {
        match self {
            Self::Copy => v,
            Self::Sqrt => v.sqrt(),
            Self::Sin => v.sin(),
            Self::Cos => v.cos(),
            Self::Tan => v.tan(),
            Self::Round => v.round(),
            Self::Floor => v.floor(),
            Self::Ceil => v.ceil(),
            Self::Signum => v.signum(),
            Self::Fract => v.fract(),
            Self::Abs => v.abs(),
            Self::Recip => v.recip(),
            Self::Trunc => v.trunc(),
            Self::Asin => v.asin(),
            Self::Acos => v.acos(),
            Self::Atan => v.atan(),
            Self::Sinh => v.sinh(),
            Self::Cosh => v.cosh(),
            Self::Tanh => v.tanh(),
            Self::Asinh => v.asinh(),
            Self::Acosh => v.acosh(),
            Self::Atanh => v.atanh(),
            Self::ToRadians => v.to_radians(),
            Self::ToDegrees => v.to_degrees(),
            Self::Invert => 1.0 - v,
            Self::Negate => -v,
        }
    }
}
/// ALU2 op
#[derive(Clone, Copy)]
pub enum ProgramALU2 {
    Add,
    Sub,
    Div,
    Mul,
    Pow,
    Max,
    Min,
    Atan2
}
impl ProgramALU2 {
    pub fn by_str(name: &str) -> Option<ProgramALU2> {
        if name.eq("add") {
            Some(ProgramALU2::Add)
        } else if name.eq("sub") {
            Some(ProgramALU2::Sub)
        } else if name.eq("div") {
            Some(ProgramALU2::Div)
        } else if name.eq("mul") {
            Some(ProgramALU2::Mul)
        } else if name.eq("pow") {
            Some(ProgramALU2::Pow)
        } else if name.eq("max") {
            Some(ProgramALU2::Max)
        } else if name.eq("min") {
            Some(ProgramALU2::Min)
        } else if name.eq("atan2") {
            Some(ProgramALU2::Atan2)
        } else {
            None
        }
    }
    #[inline]
    pub fn eval(&self, a: f64, b: f64) -> f64 {
        match self {
            Self::Add => a + b,
            Self::Sub => a - b,
            Self::Div => a / b,
            Self::Mul => a * b,
            Self::Pow => a.powf(b),
            Self::Max => a.max(b),
            Self::Min => a.min(b),
            Self::Atan2 => a.atan2(b),
        }
    }
}
/// Sequence of steps.
pub type Program = Vec<ProgramStep>;

/// Everything the program needs.
pub struct ProgramEnv {
    /// Reference instant.
    pub time_mono_instant_ref: Instant,
    /// Env monotonic time
    pub time_mono: f64,
    /// Env epoch time
    pub time_epoch: f64,
    /// Env epoch time, in Rust terms
    pub time_epoch_rs: SystemTime,
    /// OSC bus queues
    pub osc_queues: Vec<Vec<OscMessage>>,
    /// Data store
    pub store: ProgramStore,
    /// OSC message
    pub osc_message: OscMessage,
    /// OSC bundler
    /// The program runner does NOT auto-flush this!
    /// The caller should flush this at the end of the tick program.
    pub osc_bundler: OscBundler
}

impl ProgramEnv {
    pub fn new(config: &JsonValue, queues: usize) -> ProgramEnv {
        let mut floats = vec![];
        for v in config["floats_init"].members() {
            floats.push(v.as_f64().unwrap());
        }
        let mut osc_queues = vec![];
        for _ in 0..queues {
            osc_queues.push(vec![]);
        }
        return ProgramEnv {
            time_mono_instant_ref: Instant::now(),
            time_mono: 0.0,
            time_epoch: 0.0,
            time_epoch_rs: SystemTime::UNIX_EPOCH,
            osc_queues,
            store: ProgramStore {
                generation: config["vm_generation"].as_u64().unwrap(),
                floats
            },
            osc_message: OscMessage {
                addr: "/uninitialized".to_string(),
                args: vec![]
            },
            osc_bundler: OscBundler::new()
        };
    }
}

/// Program data store.
#[derive(Clone)]
pub struct ProgramStore {
    /// Generation ID -- used to prevent crosstalk and other issues
    pub generation: u64,
    pub floats: Vec<f64>
}

impl ProgramStore {
    pub fn copy_from(&mut self, other: &ProgramStore) {
        self.generation = other.generation;
        self.floats.resize(other.floats.len(), 0.0);
        self.floats.copy_from_slice(&other.floats);
    }
}

impl ProgramEnv {
    /// Update timers.
    pub fn update_time(&mut self) {
        self.time_mono = self.time_mono_instant_ref.elapsed().as_secs_f64();
        let now = SystemTime::now();
        self.time_epoch_rs = now;
        self.time_epoch = now.duration_since(SystemTime::UNIX_EPOCH).unwrap_or(Duration::ZERO).as_secs_f64();
    }
}

pub fn decode_program(json: &JsonValue) -> Program {
    match json {
        JsonValue::Array(vec) => {
            vec.iter().map(|v| ProgramStep::decode(v)).collect()
        },
        _ => vec![]
    }
}

impl ProgramStep {
    pub fn decode(json: &JsonValue) -> ProgramStep {
        let op = json[0].as_str().unwrap();
        if op.eq("nop") {
            ProgramStep::Nop
        } else if let Some(op) = ProgramCondition::by_str(op) {
            ProgramStep::If(op, json[1].as_usize().unwrap(), decode_program(&json[2]), decode_program(&json[3]))
        } else if let Some(op) = ProgramALU0::by_str(op) {
            ProgramStep::ALU0(json[1].as_usize().unwrap(), op)
        } else if let Some(op) = ProgramALU1::by_str(op) {
            ProgramStep::ALU1(json[1].as_usize().unwrap(), op, json[2].as_usize().unwrap())
        } else if let Some(op) = ProgramALU2::by_str(op) {
            ProgramStep::ALU2(json[1].as_usize().unwrap(), op, json[2].as_usize().unwrap(), json[3].as_usize().unwrap())
        } else if json[0].eq("osc_create") {
            ProgramStep::CreateOSCMessage(ProgramOSCSchematic::decode(&json[1]))
        } else if json[0].eq("osc_forward") {
            ProgramStep::ForwardOSCMessage(json[1].as_usize().unwrap())
        } else if json[0].eq("osc_decode") {
            ProgramStep::DecodeOSCMessage(ProgramOSCSchematic::decode(&json[1]), decode_program(&json[2]), decode_program(&json[3]))
        } else if json[0].eq("osc_send") {
            let s = json[1].as_str().unwrap();
            if let Ok(sa) = SocketAddr::from_str(s) {
                ProgramStep::SendOSCMessage(sa)
            } else {
                eprintln!("Invalid target OSC socket address {}", s);
                ProgramStep::Nop
            }
        } else {
            panic!("fed invalid bytecode");
        }
    }
}

/// Limits measured from a program.
#[derive(Clone, Copy)]
pub struct ProgramLimits {
    pub floats_len: usize,
    pub osc_queue_len: usize
    // When adding to this, ensure that VerifiedProgram::run checks these limits.
}

impl ProgramLimits {
    /// Create limits by measuring a program.
    pub fn new(prg: &Program) -> ProgramLimits {
        let mut limits = ProgramLimits {
            floats_len: 0,
            osc_queue_len: 0
        };
        limits.measure(prg);
        limits
    }
    /// Measures a program, applies to limits.
    pub fn measure(&mut self, prg: &Program) {
        for v in prg {
            self.measure_step(v);
        }
    }
    /// Measures a program step, applies to limits.
    pub fn measure_step(&mut self, prg: &ProgramStep) {
        match prg {
            ProgramStep::Nop => {},
            ProgramStep::If(_, slot, yes, no) => {
                self.measure_float(*slot);
                self.measure(yes);
                self.measure(no);
            },
            ProgramStep::ALU0(a, _) => {
                self.measure_float(*a);
            },
            ProgramStep::ALU1(a, _, b) => {
                self.measure_float(*a);
                self.measure_float(*b);
            },
            ProgramStep::ALU2(a, _, b, c) => {
                self.measure_float(*a);
                self.measure_float(*b);
                self.measure_float(*c);
            },
            ProgramStep::CreateOSCMessage(oscc) => {
                for v in 0..oscc.args_len {
                    self.measure_float(oscc.args_start + v);
                }
            },
            ProgramStep::ForwardOSCMessage(queue) => {
                self.measure_osc_queue(*queue);
            },
            ProgramStep::DecodeOSCMessage(oscc, yes, no) => {
                for v in 0..oscc.args_len {
                    self.measure_float(oscc.args_start + v);
                }
                self.measure(yes);
                self.measure(no);
            },
            ProgramStep::SendOSCMessage(_) => {
                // nothing to do
            },
        }
    }
    /// Measures a float.
    pub fn measure_float(&mut self, slot: usize) {
        if slot == usize::max_value() {
            panic!("slot == usize::max_value() so cannot safely calc required len!");
        }
        let res = slot + 1;
        if self.floats_len < res {
            self.floats_len = res;
        }
    }
    /// Measures a queue.
    pub fn measure_osc_queue(&mut self, slot: usize) {
        if slot == usize::max_value() {
            panic!("slot == usize::max_value() so cannot safely calc required len!");
        }
        let res = slot + 1;
        if self.osc_queue_len < res {
            self.osc_queue_len = res;
        }
    }
}

/// A verified program performs all bounds checks in advance.
/// This allows for faster execution.
#[derive(Clone)]
pub struct VerifiedProgram {
    program: Program,
    limits: ProgramLimits,
}

impl VerifiedProgram {
    /// Measures limits and creates a VerifiedProgram as a result.
    pub fn verify(prg: &Program) -> VerifiedProgram {
        VerifiedProgram { program: prg.clone(), limits: ProgramLimits::new(prg) }
    }
    /// Runs bounds checks and then runs the program.
    pub fn run(&self, env: &mut ProgramEnv) {
        if env.store.floats.len() < self.limits.floats_len {
            panic!("Cannot run this VerifiedProgram ; Float storage length is too short. Is the bytecode correct?");
        }
        if env.osc_queues.len() < self.limits.osc_queue_len {
            panic!("Cannot run this VerifiedProgram ; OSC queue length is too short. Is the bytecode correct?");
        }
        // SAFETY: Above checks ensure program limits cannot be violated.
        unsafe {
            run_program_unsafe(&self.program, env);
        }
    }
}

/// Main runtime outer loop.
/// SAFETY: You must check the environment exceeds the program's limits.
unsafe fn run_program_unsafe(prg: &Program, env: &mut ProgramEnv) {
    for v in prg {
        run_program_step_unsafe(v, env)
    }
}

/// Main runtime inner.
/// SAFETY: You must check the environment exceeds the program's limits.
#[inline]
unsafe fn run_program_step_unsafe(prg: &ProgramStep, env: &mut ProgramEnv) {
    match prg {
        ProgramStep::Nop => {},
        ProgramStep::If(cond, v, yes, no) => {
            run_program_unsafe(if cond.eval(*env.store.floats.get_unchecked(*v)) {
                yes
            } else {
                no
            }, env);
        },
        ProgramStep::ALU0(d, op) => {
            (*env.store.floats.get_unchecked_mut(*d)) = op.eval(env);
        },
        ProgramStep::ALU1(d, op, v) => {
            (*env.store.floats.get_unchecked_mut(*d)) = op.eval(*env.store.floats.get_unchecked(*v));
        },
        ProgramStep::ALU2(d, op, a, b) => {
            (*env.store.floats.get_unchecked_mut(*d)) = op.eval(*env.store.floats.get_unchecked(*a), *env.store.floats.get_unchecked(*b));
        },
        ProgramStep::CreateOSCMessage(schematic) => {
            let argvec: Vec<OscType> = schematic.copy_from_store(&env.store);
            env.osc_message = OscMessage { addr: schematic.method.clone(), args: argvec };
        },
        ProgramStep::ForwardOSCMessage(queue) => {
            let q2 = &mut env.osc_queues.get_unchecked_mut(*queue);
            q2.push(env.osc_message.clone());
        },
        ProgramStep::DecodeOSCMessage(schematic, yes, no) => {
            run_program_unsafe(if env.osc_message.addr.eq(&schematic.method) && schematic.copy_into_store(&mut env.store, &env.osc_message.args) {
                yes
            } else {
                no
            }, env);
        },
        ProgramStep::SendOSCMessage(sa) => {
            env.osc_bundler.push(*sa, env.osc_message.clone());
        }
    }
}
