use std::{io::Write, net::{TcpListener, TcpStream}, sync::Arc};

use crate::hub::Hub;

fn read_u32<R: std::io::Read>(src: &mut R) -> std::io::Result<u32> {
    let mut data: [u8; 4] = [0; 4];
    _ = src.read_exact(&mut data)?;
    Ok(u32::from_le_bytes(data))
}

fn read_u64<R: std::io::Read>(src: &mut R) -> std::io::Result<u64> {
    let mut data: [u8; 8] = [0; 8];
    _ = src.read_exact(&mut data)?;
    Ok(u64::from_le_bytes(data))
}

fn read_f64<R: std::io::Read>(src: &mut R) -> std::io::Result<f64> {
    let mut data: [u8; 8] = [0; 8];
    _ = src.read_exact(&mut data)?;
    Ok(f64::from_le_bytes(data))
}

fn read_godot_string<R: std::io::Read>(src: &mut R) -> std::io::Result<String> {
    let len = read_u32(src)?;
    let mut data = Vec::new();
    data.resize(len as usize, 0);
    src.read_exact(&mut data)?;
    String::from_utf8(data).map_err(|_| std::io::ErrorKind::InvalidData.into())
}

fn write_u32<W: std::io::Write>(dst: &mut W, val: u32) -> std::io::Result<()> {
    dst.write_all(&val.to_le_bytes())
}

fn write_u64<W: std::io::Write>(dst: &mut W, val: u64) -> std::io::Result<()> {
    dst.write_all(&val.to_le_bytes())
}

fn write_godot_string<W: std::io::Write>(dst: &mut W, txt: &str) -> std::io::Result<()> {
    write_u32(dst, txt.as_bytes().len() as u32)?;
    dst.write_all(txt.as_bytes())
}

fn connection_thread(hub: Arc<Hub>, mut control: TcpStream) -> Option<()> {
    if !read_godot_string(&mut control).ok()?.eq(&hub.password) {
        // invalid password
        return write_godot_string(&mut control, "wrong password").ok();
    }
    write_godot_string(&mut control, "OSCGraphEditor").ok()?;
    loop {
        let cmd = read_godot_string(&mut control).ok()?;
        if cmd.eq("store_download") {
            write_godot_string(&mut control, "ok").ok()?;
            let store = hub.store.lock().unwrap().clone();
            let mut data = vec![];
            write_u64(&mut control, store.generation).ok()?;
            write_u64(&mut control, store.floats.len() as u64).ok()?;
            for v in store.floats {
                data.write_all(&v.to_le_bytes()).ok()?;
            }
            control.write_all(&data).ok()?;
        } else if cmd.eq("cfg_upload") {
            let cfg = read_godot_string(&mut control).ok()?;
            let data = json::parse(&cfg);
            if let Ok(res) = data {
                let gen = hub.set_config(res);
                write_godot_string(&mut control, "ok").ok()?;
                write_u64(&mut control, gen).ok()?;
            } else {
                write_godot_string(&mut control, "parse error").ok()?;
            }
        } else if cmd.eq("cfg_download") {
            write_godot_string(&mut control, "ok").ok()?;
            write_godot_string(&mut control, &hub.config().dump()).ok()?;
        } else if cmd.eq("poke") {
            let vmgen = read_u64(&mut control).ok()?;
            let addr = read_u32(&mut control).ok()?;
            let value = read_f64(&mut control).ok()?;
            _ = hub.nexus_input.send(crate::hub::HubMessage::FloatPoke(vmgen, vec![(addr as usize, value)]));
            write_godot_string(&mut control, "ok").ok()?;
        } else {
            return write_godot_string(&mut control, "unknown command").ok();
        }
    }
}

pub fn server_thread(hub: Arc<Hub>, tl: TcpListener) {
    loop {
        let control = tl.accept().unwrap().0;
        let hub_clone = hub.clone();
        std::thread::spawn(move || {
            connection_thread(hub_clone, control);
        });
    }
}
