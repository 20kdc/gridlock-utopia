# Monado In Flatpak Steam

Here because I don't know where else to put it.

Unfortunately the target application (VRChat) doesn't actually seem to work with OpenComposite.

## You Will Need To Supply The Monado Client Yourself

Ok, so here's where things get "fun".

You should have Monado working outside of Flatpak Steam with the patched OpenHMD _first_ before trying this.

_Use as any-linux-y a build as you can. Your main risk here is glibc symbol versions._

Then you need `libopenxr_monado.so` from the build, which you will need to copy to this directory.

## The Plan

Get the Monado client to be loaded as the default OpenXR runtime inside Steam Flatpak

Supply the Monado socket inside the Steam Flatpak environment and actually make that work
