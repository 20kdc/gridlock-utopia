class_name OGEGraphNode2
extends OGEGraphNode

var compiler: OGECompiler
var cmap: OGEConnectivity

## Do postconstruction setup (i.e. hooking into "runtime_data" signal)
func _setup(graph: OGEGraph):
	._setup(graph)
	connect("resize_request", self, "__oge_resize_request_handler")

func register_for_runtime_data(graph: OGEGraph):
	graph.connect("runtime_data", self, "_runtime_data")

func _runtime_data(_store: OGEStore):
	pass

func set_resources(resources: Array):
	clear_all_slots()
	left_slots_by_index = {}
	right_slots_by_index = {}
	input_slots_by_name = {}
	output_slots_by_name = {}
	# autoassign godot's port index system which mismatches everything
	# and makes code unnecessarily complicated for no good reason
	# (seriously, why are there TWO SEPARATE KINDS OF PORT INDEX?)
	# (actually three if you count left/right separation)
	var left_slot_count := 0
	var right_slot_count := 0
	resources_list = []
	resources_map = {}
	for slot in resources:
		var res: OGEBaseResourceHolder = slot
		assert(not res.name in resources_map, "resource name conflict: " + res.name)
		resources_map[res.name] = res
		resources_list.push_back(res)
		if slot is OGESlotInfo:
			if slot.port_right:
				slot.port_index = right_slot_count
				right_slot_count += 1
				right_slots_by_index[slot.port_index] = slot
				set_slot_enabled_right(slot.element_index, true)
				set_slot_color_right(slot.element_index, slot.colour)
				set_slot_type_right(slot.element_index, slot.type)
			else:
				slot.port_index = left_slot_count
				left_slot_count += 1
				left_slots_by_index[slot.port_index] = slot
				set_slot_enabled_left(slot.element_index, true)
				set_slot_color_left(slot.element_index, slot.colour)
				set_slot_type_left(slot.element_index, slot.type)
			if slot is OGEInputInfo:
				input_slots_by_name[slot.name] = slot
			elif slot is OGEOutputInfo:
				output_slots_by_name[slot.name] = slot
	emit_signal("please_fix_connectivity")

func __oge_resize_request_handler(newsize):
	rect_size = newsize

## Compiler calls this to start the compilation.
## It initializes base2 stuff.
func _compile_start(comp: OGECompiler):
	self.compiler = comp
	self.cmap = comp.cmap
	for v in resources_list:
		(v as OGEBaseResourceHolder).resource = -1

## Allocates a named float.
func alloc_float(rh: OGEBaseResourceHolder, sym: String, def: float = 0.0):
	compiler.alloc_float(rh, name + "/" + sym, def)

## Handles everything you'd usually worry about.
func compile_get_input_float(slot: OGEInputInfo, def: float = 0.0) -> int:
	if slot.resource == -1:
		compiler.alloc_float(slot, name + "/" + slot.name, def)
	return slot.resource

## Writes into dictionary
func _serialize(props: Dictionary):
	props["_type"] = editor_type_id
	props["_name"] = name
	props["_ox"] = offset.x
	props["_oy"] = offset.y
	props["_sx"] = rect_size.x
	props["_sy"] = rect_size.y
	for r2 in resources_list:
		var res: OGEBaseResourceHolder = r2
		if res.resource != -1:
			props["_r/" + res.name] = res.resource
		if res is OGEInputInfo:
			if res.source != null:
				props["_c/" + res.name] = res.source.string

## Reads from dictionary
func _deserialize(props: Dictionary):
	name = props["_name"]
	if props.has("_ox") and props.has("_oy"):
		offset = Vector2(props["_ox"], props["_oy"])
	if props.has("_sx") and props.has("_sy"):
		rect_size = Vector2(props["_sx"], props["_sy"])
	for r2 in resources_list:
		var res: OGEBaseResourceHolder = r2
		var v = props.get("_r/" + res.name)
		if v != null:
			res.resource = v

## Reads from dictionary, specifically fills slot_sources
## Assumes subclass may have updated index/slot relationships in _deserialize
func _deserialize_connections(props: Dictionary):
	for v in input_slots_by_name:
		var res = props.get("_c/" + v)
		if res is String:
			input_slot_by_name(v).source = OGESlotRef.new(res, "", true)
