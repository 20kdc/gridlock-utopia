class_name OGESlotRef
extends Reference

# Node name
export var name: String setget _fail
# Slot name
export var slot: String setget _fail
# Slot name
export var string: String setget _fail

## Init
func _init(xname: String = "", xslot: String = "", parse: bool = false):
	if parse:
		var pos = xname.find("/")
		if pos == -1:
			name = xname
			slot = ""
		else:
			name = xname.substr(0, pos)
			slot = xname.substr(pos + 1)
	else:
		name = xname
		slot = xslot
	string = name + "/" + slot

## convert slotref to string (JSON)
func _to_string():
	return string

## fail to set
func _fail(_arg):
	assert(false, "cannot set name/slot of OGESlotRef")
