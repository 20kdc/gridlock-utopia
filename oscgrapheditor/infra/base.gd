# interface class for use by OGECompiler, OGEGraph
# impl in OGEGraphNode2
class_name OGEGraphNode
extends GraphNode

## Editor's type ID
var editor_type_id := "UNKNOWN"

# GraphNode's slot index design is dumb and bad
var left_slots_by_index := {}
var right_slots_by_index := {}
# used for connectivity analysis
var input_slots_by_name := {}
var output_slots_by_name := {}

## All OGEBaseResourceHolders
var resources_list := []
var resources_map := {}

## Does this node tick?
var ticking := false

signal please_resave_graph()
signal please_fix_connectivity()

## Do postconstruction setup (i.e. hooking into "runtime_data" signal)
func _setup(_graph):
	pass

func contents_changed():
	emit_signal("please_resave_graph")

## base2 only
func __base2_please_fix_connectivity():
	emit_signal("please_fix_connectivity")

## Compiler calls this to start the compilation.
## It initializes base2 stuff and sets up the slot list.
func _compile_start(_compiler):
	pass

## Pass which creates sockets (OSC queues etc.)
func _compile_pass_init_sockets():
	pass

## Pass which creates values
func _compile_pass_init_values():
	pass

## Pass which links resources - must return true if and only if it did anything
##  visible.
func _compile_pass_link_resources() -> bool:
	return false

## Pass which links OSC methods to their inputs/outputs
func _compile_pass_link_methods():
	pass

# Writes tick bytecode for this node.
# tick_start -> calc -> tick -> tick_end
#
# For nodes that have outputs and tick (and therefore must buffer):
#  tick_start loads outputs from last frame
#  calc and tick do nothing (and calc acts weird due to dependency barriers)
#  tick_end is where calculations are done
#
# A pretty simple example of why this is relevant would be a PID controller.
# You want the inputs to be ready and completely consistent when you tick it.
# But you also need its output to be consistent.
# As a result, the PID controller node must be buffered.

func _compile_write_tick_start(_bytecode: Array):
	pass
func _compile_write_calc(_bytecode: Array):
	pass
func _compile_write_tick(_bytecode: Array):
	pass
func _compile_write_tick_end(_bytecode: Array):
	pass

## Writes into dictionary
func _serialize(_props: Dictionary):
	pass

## Reads from dictionary
func _deserialize(_props: Dictionary):
	pass

## Reads from dictionary, specifically fills slot_sources
## Assumes subclass may have updated index/slot relationships in _deserialize
func _deserialize_connections(_props: Dictionary):
	pass

# -- Useful Accessors --

func input_slot_by_name(name: String) -> OGEInputInfo:
	return input_slots_by_name.get(name)
func output_slot_by_name(name: String) -> OGEOutputInfo:
	return output_slots_by_name.get(name)

func left_slot_by_index(idx: int) -> OGESlotInfo:
	return left_slots_by_index.get(idx)
func right_slot_by_index(idx: int) -> OGESlotInfo:
	return right_slots_by_index.get(idx)
