class_name OGEStore
extends Reference

var floats: StreamPeerBuffer
var float_count := 0

func _init():
	floats = StreamPeerBuffer.new()
	floats.big_endian = false

func set_floats(data: PoolByteArray):
	floats.data_array = data
	# this integer division is intentional
	float_count = len(data) / 8

func get_float(resource: OGEBaseResourceHolder, def: float = 0.0) -> float:
	return get_float_idx(resource.resource, def)

func get_float_idx(resource: int, def: float = 0.0) -> float:
	if resource >= 0 and resource < float_count:
		floats.seek(resource * 8)
		return floats.get_double()
	return def
