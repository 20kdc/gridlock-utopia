class_name OGESlotInfo
extends OGEBaseResourceHolder

## Type
var type: int setget _fail
## Index
var element_index: int setget _fail
var port_right: bool setget _fail
var port_index: int
## Colour
var colour: Color setget _fail

## Init
func _init_slot_info(xei: int, right_side: bool, xname: String, xtype: int):
	_init_resource_holder(xname)
	element_index = xei
	port_right = right_side
	port_index = -1
	type = xtype
	colour = OGEResourceHolder.type_to_colour(xtype)

## fail to set
func _fail(_arg):
	assert(false, "cannot set name/type/index/side of slot info")
