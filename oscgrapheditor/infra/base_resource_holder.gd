class_name OGEBaseResourceHolder
extends Reference

## Resource Name -- unique within a graph node
var name: String setget _fail

# f64 (runtime)
const SLOT_TYPE_FLOAT = 0
# OSC bus (comptime)
const SLOT_TYPE_OSC = 1

## Resource ID (updated on recompile)
var resource: int = -1

func _init_resource_holder(xname: String):
	name = xname

static func type_to_colour(type: int) -> Color:
	if type == SLOT_TYPE_FLOAT:
		return Color.white
	elif type == SLOT_TYPE_OSC:
		return Color.red
	return Color.magenta

## fail to set
func _fail(_arg):
	assert(false, "cannot set name of resource holder")
