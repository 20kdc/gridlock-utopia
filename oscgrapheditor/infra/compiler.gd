class_name OGECompiler
extends Reference

const PASS_UNINITIALIZED = 0
const PASS_INIT_SOCKETS = 1
const PASS_INIT_VALUES = 2
const PASS_LINK_RESOURCES = 3
const PASS_LINK_METHODS = 4
const PASS_WRITE_BYTECODE = 5
const PASS_COMPLETE = 6

var compile_pass = PASS_UNINITIALIZED

# this is in config's form
var floats_init: Array = []
var floats_sym: Array = [] # debug
var buses: Array = []
var threads: Array = []
var tick: Array = []

var cmap: OGEConnectivity

func _init(ge: GraphEdit):
	cmap = OGEConnectivity.new(ge)

# -- Compile Function --

func compile():
	# init resources
	for v in cmap.nodelist:
		var gn: OGEGraphNode = v
		# clears resource values to -1
		gn._compile_start(self)
	compile_pass = PASS_INIT_SOCKETS
	for v in cmap.nodelist:
		var gn: OGEGraphNode = v
		gn._compile_pass_init_sockets()
	compile_pass = PASS_INIT_VALUES
	for v in cmap.nodelist:
		var gn: OGEGraphNode = v
		gn._compile_pass_init_values()
	compile_pass = PASS_LINK_RESOURCES
	while true:
		var did_anything = false
		for v in cmap.nodelist:
			var gn: OGEGraphNode = v
			for k in gn.input_slots_by_name:
				var si := gn.input_slot_by_name(k)
				if si.resource != -1:
					continue
				if si.source != null:
					var so := cmap.output_slot_by_ref(si.source)
					if so != null:
						if so.type == si.type:
							si.resource = so.resource
							did_anything = true
			if gn._compile_pass_link_resources():
				did_anything = true
		if not did_anything:
			break
	compile_pass = PASS_LINK_METHODS
	for v in cmap.nodelist:
		var gn: OGEGraphNode = v
		gn._compile_pass_link_methods()
	compile_pass = PASS_WRITE_BYTECODE
	# figure out which nodes we actually want to tick
	var ticking := []
	for v in cmap.nodelist:
		var gn: OGEGraphNode = v
		if gn.ticking:
			ticking.push_back(gn)
		elif cmap.dependents[gn.name].empty():
			ticking.push_back(gn)
	# buffers
	for v in ticking:
		(v as OGEGraphNode)._compile_write_tick_start(tick)
	# run all calcs
	var did_calc := {}
	for v in ticking:
		_write_calc_bytecode(tick, did_calc, (v as OGEGraphNode).name, false)
	# *then* tick (this is important, see OGEGraphNode::_compile_write_tick)
	# everything else
	for v in ticking:
		(v as OGEGraphNode)._compile_write_tick(tick)
	# buffers
	for v in ticking:
		(v as OGEGraphNode)._compile_write_tick_end(tick)
	compile_pass = PASS_COMPLETE

func _write_calc_bytecode(bytecode: Array, did_calc: Dictionary, name: String, ignore_ticking: bool):
	# print("writing calc bytecode: " + name)
	# in the case of cycles, this will break at an arbitrary point
	# otherwise, it should be totally fine
	if name in did_calc:
		return
	var gn := cmap.get_node(name)
	if gn.ticking and ignore_ticking:
		# these are considered 'buffered' and are thus immune
		return
	did_calc[name] = true
	for v in cmap.dependencies[name]:
		_write_calc_bytecode(bytecode, did_calc, v, true)
	# now write our bytecode
	gn._compile_write_calc(bytecode)

# -- Resources --

func alloc_float(rh: OGEBaseResourceHolder, reason: String, def: float = 0.0):
	floats_init.push_back(def)
	floats_sym.push_back(reason)
	rh.resource = len(floats_init) - 1

func alloc_osc_bus(rh: OGEBaseResourceHolder):
	assert(compile_pass == PASS_INIT_SOCKETS)
	buses.push_back([])
	rh.resource = len(buses) - 1
