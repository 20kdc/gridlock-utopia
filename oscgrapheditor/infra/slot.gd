class_name OGESlot
extends Reference

static func input_float(ei: int, name: String) -> OGEInputInfo:
	return OGEInputInfo.new(ei, false, name, OGEInputInfo.SLOT_TYPE_FLOAT)
static func output_float(ei: int, name: String) -> OGEOutputInfo:
	return OGEOutputInfo.new(ei, true, name, OGEInputInfo.SLOT_TYPE_FLOAT)

static func input_osc_left(ei: int, name: String) -> OGEInputInfo:
	return OGEInputInfo.new(ei, false, name, OGEInputInfo.SLOT_TYPE_OSC)
static func input_osc_right(ei: int, name: String) -> OGEInputInfo:
	return OGEInputInfo.new(ei, true, name, OGEInputInfo.SLOT_TYPE_OSC)

static func output_osc_left(ei: int, name: String) -> OGEOutputInfo:
	return OGEOutputInfo.new(ei, false, name, OGEInputInfo.SLOT_TYPE_OSC)
static func output_osc_right(ei: int, name: String) -> OGEOutputInfo:
	return OGEOutputInfo.new(ei, true, name, OGEInputInfo.SLOT_TYPE_OSC)
