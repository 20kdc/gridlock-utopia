class_name OGEConnectivity
extends Reference

# Maps "NodeName/slot" to a list of SlotRef targets
var targets: Dictionary
var nodemap: Dictionary
# Maps node names to PoolStringArrays of other node names.
var dependencies: Dictionary
# Maps node names to PoolStringArrays of other node names.
var dependents: Dictionary
var nodelist: Array

func _init(ge: GraphEdit):
	targets = {}
	nodemap = {}
	for v in ge.get_children():
		if v is OGEGraphNode:
			dependencies[v.name] = {}
			dependents[v.name] = {}
			nodemap[v.name] = v
			for v2 in v.output_slots_by_name:
				var ref := OGESlotRef.new(v.name, v2)
				targets[ref.string] = []
	nodelist = nodemap.values()
	for v in nodelist:
		var gn: OGEGraphNode = v
		for slot in gn.input_slots_by_name:
			var f := gn.input_slot_by_name(slot).source
			if f != null:
				var gnf := get_node(f.name)
				if gnf == null:
					continue
				var t: OGESlotRef = OGESlotRef.new(gn.name, slot)
				targets[f.string].push_back(t)
				# depgraph is just node names
				# print("DEP " + t.name + " - " + f.name)
				dependencies[t.name][f.name] = true
				dependents[f.name][t.name] = true

## Gets a node by name.
func get_node(name: String) -> OGEGraphNode:
	return nodemap[name]

func input_slot_by_ref(ref: OGESlotRef) -> OGEInputInfo:
	var res: OGEGraphNode = nodemap.get(ref.name)
	if res == null:
		return null
	return res.input_slots_by_name[ref.slot]

func output_slot_by_ref(ref: OGESlotRef) -> OGEOutputInfo:
	var res: OGEGraphNode = nodemap.get(ref.name)
	if res == null:
		return null
	return res.output_slots_by_name[ref.slot]
