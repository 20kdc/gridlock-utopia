class_name OGEOutputInfo
extends OGESlotInfo

## Init
func _init(xei: int, right_side: bool, xname: String, xtype: int):
	_init_slot_info(xei, right_side, xname, xtype)

## fail to set
func _fail(_arg):
	assert(false, "cannot set name/type/index of slot info")
