extends OGEGraphNode2

var slot_enabled := OGESlot.input_float(0, "enabled")
var slot_bus := OGESlot.input_osc_right(1, "bus")

var slots := []
var slots_mirror := []
var labels := []

func _setup(graph: OGEGraph):
	._setup(graph)
	ticking = true
	_rebuild_slots()

func _serialize(props: Dictionary):
	._serialize(props)
	props["method"] = $path.text
	props["length"] = $SpinBox.value

func _deserialize(props: Dictionary):
	._deserialize(props)
	$path.text = props["method"]
	$SpinBox.value = int(props["length"])

func _rebuild_slots():
	var arr := [slot_enabled, slot_bus]
	for v in labels:
		v.queue_free()
	labels = []
	slots = []
	slots_mirror = []
	for i in range(int($SpinBox.value)):
		var label = Label.new()
		label.text = str(i)
		add_child(label)
		labels.push_back(label)
		var argid := "a" + str(i)
		var slot := OGESlot.input_float(3 + i, argid)
		arr.push_back(slot)
		slots.push_back(slot)
		slots_mirror.push_back(OGEResourceHolder.new(argid))
	set_resources(arr)

func _compile_pass_init_values():
	# allocate, and in-order
	var i := 0
	for v in slots_mirror:
		alloc_float(v, "a" + str(i))
		i += 1

func _compile_write_tick(bytecode: Array):
	if slot_bus.resource != -1:
		var subcode := []
		for i in range(len(slots)):
			var res: int = slots[i].resource
			if res != -1:
				subcode.push_back(["copy", slots_mirror[i].resource, res])
		var res_first := 0
		if len(slots_mirror) > 0:
			res_first = slots_mirror[0].resource
		subcode.push_back(["osc_create", [$path.text, res_first, len(slots_mirror)]])
		subcode.push_back(["osc_forward", slot_bus.resource])
		if slot_enabled.resource != -1:
			bytecode.push_back(["if_!=0", slot_enabled.resource, subcode, []])
		else:
			bytecode.append_array(subcode)

func _on_SpinBox_value_changed(_value):
	_rebuild_slots()
	contents_changed()

func _on_path_text_entered(_new_text):
	contents_changed()
