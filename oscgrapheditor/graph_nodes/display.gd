extends OGEGraphNode2

var slot_value := OGESlot.input_float(0, "value")

func _setup(graph: OGEGraph):
	._setup(graph)
	register_for_runtime_data(graph)
	set_resources([slot_value])
	ticking = true

func _runtime_data(store: OGEStore):
	var val := store.get_float(slot_value)
	$"TabContainer/0-1".value = val
	$"TabContainer/text".text = str(val)
	$"TabContainer/rad/c".rect_rotation = rad2deg(val)

func _serialize(props):
	._serialize(props)
	props["tab"] = $TabContainer.current_tab

func _deserialize(props):
	._deserialize(props)
	if props.has("tab"):
		$TabContainer.current_tab = props["tab"]
