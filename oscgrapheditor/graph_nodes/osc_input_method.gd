extends OGEGraphNode2

var slot_bus := OGESlot.input_osc_left(0, "bus")

var slots := []
var labels := []

func _setup(graph: OGEGraph):
	._setup(graph)
	_rebuild_slots()

func _serialize(props: Dictionary):
	._serialize(props)
	props["method"] = $path.text
	props["length"] = $SpinBox.value

func _deserialize(props: Dictionary):
	._deserialize(props)
	$path.text = props["method"]
	$SpinBox.value = int(props["length"])

func _rebuild_slots():
	var arr := [slot_bus]
	for v in labels:
		v.queue_free()
	labels = []
	slots = []
	for i in range(int($SpinBox.value)):
		var label = Label.new()
		label.text = str(i)
		add_child(label)
		labels.push_back(label)
		var slot := OGESlot.output_float(2 + i, "a" + str(i))
		arr.push_back(slot)
		slots.push_back(slot)
	set_resources(arr)

func _compile_pass_init_values():
	# allocate, and in-order
	var i := 0
	for v in slots:
		alloc_float(v, "a" + str(i))
		i += 1

func _compile_pass_link_methods():
	if slot_bus.resource != -1:
		var res_first := 0
		if len(slots) > 0:
			res_first = slots[0].resource
		compiler.buses[slot_bus.resource].push_back(["osc_decode", [$path.text, res_first, len(slots)], [], []])

func _on_SpinBox_value_changed(_value):
	_rebuild_slots()
	contents_changed()

func _on_path_text_entered(_new_text):
	contents_changed()
