extends OGEALUCommonNode

func _setup(graph: OGEGraph):
	._setup(graph)
	set_resources([slot_value])
	$op.setup(["time_mono", "time_epoch"])

func _compile_write_calc(bytecode: Array):
	bytecode.push_back([$op.enum_get(), slot_value.resource])
