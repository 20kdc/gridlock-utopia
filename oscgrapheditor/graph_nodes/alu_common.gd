class_name OGEALUCommonNode
extends OGEGraphNode2

var slot_value := OGESlot.output_float(0, "value")

func _setup(graph: OGEGraph):
	._setup(graph)
	register_for_runtime_data(graph)

func _runtime_data(store: OGEStore):
	var val := store.get_float(slot_value)
	$value.text = str(val)

## Writes into dictionary
func _serialize(props: Dictionary):
	._serialize(props)
	props["op"] = $op.enum_get()

## Reads from dictionary
func _deserialize(props: Dictionary):
	._deserialize(props)
	$op.enum_set(props.get("op"))

func _compile_pass_init_values():
	alloc_float(slot_value, "value")

func _on_value_item_selected(_index):
	contents_changed()
