extends OGEGraphNode2

export var is_input := false

var slot_bus: OGEInputInfo

func _setup(graph: OGEGraph):
	._setup(graph)
	if is_input:
		slot_bus = OGESlot.input_osc_right(0, "bus")
	else:
		slot_bus = OGESlot.input_osc_left(0, "bus")
	set_resources([slot_bus])

## Writes into dictionary
func _serialize(props: Dictionary):
	._serialize(props)
	props["address"] = $LineEdit.text

## Reads from dictionary
func _deserialize(props: Dictionary):
	._deserialize(props)
	$LineEdit.text = str(props["address"])

func _compile_pass_link_methods():
	if slot_bus.resource != -1:
		if is_input:
			compiler.threads.push_back(["osc_input", $LineEdit.text, slot_bus.resource])
		else:
			compiler.buses[slot_bus.resource].push_back(["osc_send", $LineEdit.text])

func _on_LineEdit_text_entered(_new_text):
	contents_changed()
