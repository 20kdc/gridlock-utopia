extends OGEALUCommonNode

var slot_a := OGESlot.input_float(0, "a")
var slot_b := OGESlot.input_float(1, "b")

func _setup(graph: OGEGraph):
	._setup(graph)
	set_resources([slot_a, slot_b, slot_value])
	$op.setup(["add", "sub", "div", "mul", "pow", "max", "min", "atan2"])

func _compile_write_calc(bytecode: Array):
	bytecode.push_back([$op.enum_get(), slot_value.resource, compile_get_input_float(slot_a), compile_get_input_float(slot_b)])
