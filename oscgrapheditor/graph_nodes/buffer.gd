extends OGEGraphNode2

var slot_input := OGESlot.input_float(0, "input")
var res_buffer := OGEResourceHolder.new("buffer")
var slot_output := OGESlot.output_float(0, "output")

func _setup(graph: OGEGraph):
	._setup(graph)
	register_for_runtime_data(graph)
	set_resources([slot_input, slot_output, res_buffer])
	ticking = true

func _runtime_data(store: OGEStore):
	var val := store.get_float(slot_output)
	$value.text = str(val)

func _compile_pass_init_values():
	alloc_float(res_buffer, "buffer")
	alloc_float(slot_output, "output")

func _compile_write_tick_start(bytecode: Array):
	bytecode.push_back(["copy", slot_output.resource, res_buffer.resource])

func _compile_write_tick_end(bytecode: Array):
	bytecode.push_back(["copy", res_buffer.resource, compile_get_input_float(slot_input)])

func _on_value_item_selected(_index):
	contents_changed()
