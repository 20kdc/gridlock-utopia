extends OGEGraphNode2

var slot_value := OGESlot.output_float(0, "value")

var cval := 0.0

func _setup(graph: OGEGraph):
	._setup(graph)
	set_resources([slot_value])

## Writes into dictionary
func _serialize(props: Dictionary):
	._serialize(props)
	props["val"] = cval

## Reads from dictionary
func _deserialize(props: Dictionary):
	._deserialize(props)
	cval = float(props["val"])
	$value.text = str(cval)

func _compile_pass_init_values():
	alloc_float(slot_value, "value", cval)

func _on_value_text_entered(new_text: String):
	if new_text.is_valid_float():
		cval = float(new_text)
		contents_changed()
	else:
		$value.text = str(cval)
