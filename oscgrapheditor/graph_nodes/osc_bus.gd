extends OGEGraphNode2

var slot_bus := OGESlot.output_osc_left(0, "bus")
var slot_bus_r := OGESlot.output_osc_right(0, "bus_r")

func _setup(graph: OGEGraph):
	._setup(graph)
	set_resources([slot_bus, slot_bus_r])

func _compile_pass_init_sockets():
	compiler.alloc_osc_bus(slot_bus)
	slot_bus_r.resource = slot_bus.resource
