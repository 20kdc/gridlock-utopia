extends OGEALUCommonNode

var slot_a := OGESlot.input_float(0, "a")

func _setup(graph: OGEGraph):
	._setup(graph)
	set_resources([slot_a, slot_value])
	$op.setup([
		"copy", "sqrt", "sin", "cos",
		"tan", "round", "floor", "ceil",
		"signum", "fract", "abs", "recip",
		"trunc", "asin", "acos", "atan",
		"sinh", "cosh", "tanh", "asinh",
		"acosh", "atanh", "to_radians", "to_degrees",
		"invert", "negate"
	])

func _compile_write_calc(bytecode: Array):
	bytecode.push_back([$op.enum_get(), slot_value.resource, compile_get_input_float(slot_a)])
