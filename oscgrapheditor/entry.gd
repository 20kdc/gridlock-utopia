extends Node

var _active_peer: StreamPeerTCP
var _active_peer_store_request = true
var _store: OGEStore = OGEStore.new()
onready var _ge: OGEGraph = $"%GraphEdit"
var _update_time = 1.0
var _update_time_acc = 0.0

func _on_Button_pressed():
	var addr: String = $"%address".text
	var port: int = $"%port".value
	var pw: String = $"%password".text
	var peer := StreamPeerTCP.new()
	if peer.connect_to_host(addr, port) != OK:
		report_status("failed: connect_to_host")
		return
	peer.put_string(pw)
	if peer.get_string() != "OSCGraphEditor":
		report_status("failed: did not accept password")
		return
	peer.put_string("cfg_download")
	if peer.get_string() != "ok":
		report_status("failed: could not retrieve config")
		return
	var parsed = JSON.parse(peer.get_string())
	if parsed.error != OK:
		report_status("failed: could not parse config")
		return
	_ge.graph_load(parsed.result)
	_on_update_time_text_entered("")
	# otherwise the async stuff fails so hard that the editor noticably lags
	peer.set_no_delay(true)
	_active_peer = peer
	_active_peer_store_request = false
	report_status("success")

func report_status(status):
	print(status)
	$"%status".text = status

func _process(delta):
	_update_time_acc += delta
	if _update_time_acc >= _update_time:
		_update_time_acc = 0.0
		# update
		if _active_peer != null:
			_complete_store_request()
		if _active_peer != null:
			_active_peer.put_string("store_download")
			_active_peer_store_request = true

func _complete_store_request():
	if _active_peer_store_request:
		_active_peer_store_request = false
		if _active_peer.get_string() != "ok":
			_active_peer = null
			return
		else:
			# vmgen
			_active_peer.get_64()
			var count := _active_peer.get_64()
			_store.set_floats(_active_peer.get_data(count * 8)[1])
			_ge.graph_pass_runtime_data(_store)

func _on_GraphEdit_saved(data):
	_complete_store_request()
	if _active_peer != null:
		_active_peer.put_string("cfg_upload")
		_active_peer.put_string(JSON.print(data))
		if _active_peer.get_string() != "ok":
			report_status("error during cfg_upload")
			_active_peer = null
		else:
			# vmgen
			_active_peer.get_64()

func _on_save_pressed():
	_ge.graph_save()

func _on_update_time_text_entered(_text):
	if $"%update_time".text.is_valid_float():
		_update_time = float($"%update_time".text)
