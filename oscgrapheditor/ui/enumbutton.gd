class_name OGEEnumButton
extends OptionButton

var _enum_options: PoolStringArray

func setup(enum_options: Array):
	_enum_options = PoolStringArray(enum_options)
	var idx = 0
	for v in _enum_options:
		add_item(v, idx)
		idx += 1
	selected = 0

func enum_get() -> String:
	return _enum_options[selected]

func enum_set(v):
	if v == null:
		selected = 0
		return
	var res = _enum_options.find(v)
	if res == -1:
		selected = 0
		return
	selected = res
