extends MenuButton

signal create_node(type)

func _ready():
	get_popup().connect("index_pressed", self, "_index_pressed")

func _index_pressed(idx: int):
	emit_signal("create_node", get_popup().get_item_text(idx))
