class_name OGEGraph
extends GraphEdit

var config_data := {}
var intended_children := []
var _clipboard = []

signal runtime_data(store)
signal saved(data)

func _init():
	connect("connection_request", self, "_connection_request")
	connect("disconnection_request", self, "_disconnection_request")
	connect("delete_nodes_request", self, "_delete_nodes_request")
	connect("copy_nodes_request", self, "_copy_nodes_request")
	connect("paste_nodes_request", self, "_paste_nodes_request")

func _connection_request(an, ai, bn, bi):
	# check the rules are being followed...

	var gna: OGEGraphNode = get_node(an)
	var ain := gna.right_slot_by_index(ai)

	var gnb: OGEGraphNode = get_node(bn)
	var bin := gnb.left_slot_by_index(bi)

	if ain == null or bin == null:
		# wtf?
		print("failed connection between " + an + ":" + str(ai) + " and " + bn + ":" + str(bi) + " because a port didn't exist")
		print(gna.right_slots_by_index.keys())
		print(gnb.left_slots_by_index.keys())
		return

	if ain.type != bin.type:
		# no!
		return

	if ain is OGEOutputInfo and bin is OGEInputInfo:
		bin.source = OGESlotRef.new(an, ain.name)
	elif ain is OGEInputInfo and bin is OGEOutputInfo:
		ain.source = OGESlotRef.new(bn, bin.name)
	else:
		# fail: input to input or output to output
		return

	_fix_connectivity()
	# aaand done?
	graph_save()

func _disconnection_request(an, ai, bn, bi):
	# check the rules are being followed...

	var gna: OGEGraphNode = get_node(an)
	var ain := gna.right_slot_by_index(ai)

	var gnb: OGEGraphNode = get_node(bn)
	var bin := gnb.left_slot_by_index(bi)

	if ain is OGEInputInfo:
		ain.source = null
	if bin is OGEInputInfo:
		bin.source = null

	_fix_connectivity()
	# aaand done?
	graph_save()

func _delete_nodes_request(_list):
	# list doesn't actually work
	var actually_delete := []
	for v in intended_children:
		if v.selected:
			actually_delete.push_back(v)
	for v in actually_delete:
		remove_child(v)
		v.queue_free()
		intended_children.erase(v)
	_fix_connectivity()
	graph_save()

func _copy_nodes_request():
	var actually_copy := []
	for v in intended_children:
		if v.selected:
			actually_copy.push_back(v)
	_clipboard = _graph_copy(actually_copy)

func _paste_nodes_request():
	for v in intended_children:
		v.selected = false
	for v in _graph_paste(_clipboard):
		v.offset += Vector2(64, 64)
		v.selected = true
	_fix_connectivity()
	graph_save()

func _fix_connectivity():
	clear_connections()
	for node in intended_children:
		var to: OGEGraphNode = node
		# .keys() is important because this also deletes broken sources
		for k in to.input_slots_by_name:
			var si := to.input_slot_by_name(k)
			var res: OGESlotRef = si.source
			if res != null:
				var from: OGEGraphNode = get_node_or_null(res.name)
				if from == null:
					si.source = null
					continue
				var oi := from.output_slot_by_name(res.slot)
				if oi == null:
					si.source = null
					continue
				# can these be connected at all???
				if si.port_right == oi.port_right:
					print("ports connected that are both on the same side. oops: ", from.name, ":", oi.name, " -> ", to.name, ":", si.name)
					si.source = null
					continue
				else:
					if oi.port_right:
						connect_node(res.name, oi.port_index, to.name, si.port_index)
					else:
						connect_node(to.name, si.port_index, res.name, oi.port_index)

func graph_pass_runtime_data(store: OGEStore):
	emit_signal("runtime_data", store)

func graph_load(data: Dictionary):
	config_data = data
	if data.has("tick_time"):
		$"%tick_time".text = str(data["tick_time"])
	if data.has("view_update_time"):
		$"%update_time".text = str(data["view_update_time"])
	clear_connections()
	for v in intended_children:
		remove_child(v)
		v.queue_free()
	intended_children.clear()
	if "nodes" in data:
		_graph_paste(data["nodes"])
	_fix_connectivity()

func _graph_copy(nodelist: Array) -> Array:
	var nodes := []
	for v in nodelist:
		var gn: OGEGraphNode = v
		var props = {}
		gn._serialize(props)
		nodes.push_back(props)
	return nodes

func _graph_paste(data: Array) -> Array:
	# this ensures duplicates get properly remapped and connections don't "hang"
	var remap := {}
	# actually do load/paste
	var nodes := []
	for v in data:
		var type: String = v["_type"]
		var original_name: String = v["_name"]
		var instance := _add_node(type)
		instance._deserialize(v)
		instance._deserialize_connections(v)
		_finish_adding_node(instance)
		remap[original_name] = instance.name
		nodes.push_back(instance)
	# remap connections to pasted nodes
	for node in nodes:
		var gn: OGEGraphNode = node
		for k in gn.input_slots_by_name:
			var v := gn.input_slot_by_name(k)
			if v.source != null:
				var remapped = remap.get(v.source.name)
				if remapped == null:
					v.source = null
				else:
					v.source = OGESlotRef.new(remapped, v.source.slot)
	return nodes

func _add_node(type: String) -> OGEGraphNode:
	var instance: OGEGraphNode = load("res://graph_nodes/" + type + ".tscn").instance()
	instance.editor_type_id = type
	instance._setup(self)
	add_child(instance)
	intended_children.push_back(instance)
	return instance

func _finish_adding_node(node: OGEGraphNode):
	node.connect("please_resave_graph", self, "graph_save")
	node.connect("please_fix_connectivity", self, "_fix_connectivity")

func graph_save():
	var compiler := OGECompiler.new(self)
	compiler.compile()
	if $"%tick_time".text.is_valid_float():
		config_data["tick_time"] = float($"%tick_time".text)
	if $"%update_time".text.is_valid_float():
		config_data["view_update_time"] = float($"%update_time".text)
	config_data["floats_init"] = compiler.floats_init
	config_data["floats_sym"] = compiler.floats_sym
	config_data["buses"] = compiler.buses
	config_data["threads"] = compiler.threads
	config_data["tick"] = compiler.tick
	config_data["nodes"] = _graph_copy(compiler.cmap.nodelist)
	emit_signal("saved", config_data)

func _on_new_create_node(type):
	var instance := _add_node(type)
	_finish_adding_node(instance)
	graph_save()
