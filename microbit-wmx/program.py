import struct
import gc
import time
from microbit import *
uart.init(115200)

tick_current = time.ticks_ms()
while True:
	x, y, z = accelerometer.get_values()
	mx, my, mz = compass.get_x(), compass.get_y(), compass.get_z()
	flags = 0
	if button_a.is_pressed():
		flags |= 1
	if button_b.is_pressed():
		flags |= 2
	uart.write(b"#" + struct.pack("<ffffffB", x, y, z, mx, my, mz, flags) + b"\n")
	# supplemental
	display.clear()
	dx = min(max(int((x / 512.0) + 2), 0), 4)
	dy = min(max(int((y / 512.0) + 2), 0), 4)
	display.set_pixel(dx, dy, 9)
	gc.collect()
	# update
	tick_next = tick_current + 10
	while time.ticks_diff(time.ticks_ms(), tick_next) < 0:
		sleep(1)
	tick_current = tick_next
