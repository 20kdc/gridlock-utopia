# Sensor Protocol

UDP port 9000 is used for OSC sensor data transmissions.

Data is always transmitted as immediate bundles, and all data is transmitted regardless of if it has changed (to act as safety in case of packet loss).

All methods in the bundle are made up of the form `/DeviceName/SensorName`, such as `/xeon/android.sensor.orientation/0`. See Sensor Methods below.

Each OSC method takes a number of floats, equivalent to the amount of floats in the sensor value.

## Sensor Types

### Android

Android sensors use the form `ANDROIDSENSORSTRINGTYPE/ANDROIDSENSORID` and use their respective Android type and sensor IDs. **If the Android device does not support `getStringType`, these are emulated. If the emulation cannot recognize the type, the number is used, i.e. `android.unknown.0` or such.**

Their floats are sent as-is.

Example name would be `android.sensor.orientation/0`.

In the case of rotation vectors, they get special treatment. These sensors have an additional method, following the pattern `android.sensor.rotation_vector/matrix/0`.

### Controller Buttons

Each controller button is considered an individual sensor. This allows them to map nicely to OSC methods and VRChat parameters.

The "controller" is `button.1` onwards. *While 0-indexing "makes more sense", using 1-indexing here aligns with the text on the actual buttons.*

### Inertial Tracking System

*This doesn't work so well but it's kept around because it might be possible to fix it (unlike the marker tracking system which was pretty much doomed).*

For all combinations of linear accelerators and rotation vectors, Inertial Tracking System 'hardware' is made available to select. 

Frankly, it's a last ditch effort to try and get something approximating hand and head positional tracking in this system.

These are represented as the method `ims/0/0`, 3 parameters for X, Y, Z. Positions are in rotation vector space.

### The Hand

The method `the_hand/pos` is used when in the "The Hand" mode of the app. This mode is similar to controller mode, but it has no buttons. Instead, it essentially acts as two very large 'sticky' thumb-sticks.

As such, it is a 4-float method, covering 2 2D vectors.

Values are from -1 to 1.

Practice with using The Hand as a head position controller has shown it shows promise, but the UX needs to be worked out in the station.
