# Serial protocol used for micro:bit "wiimote without disposable batteries" devices in Gridlock Utopia

115200 baud

* Sync start byte: `0x23`, ASCII `#`
* little-endian 32-bit float: accelerometer X
* little-endian 32-bit float: accelerometer Y
* little-endian 32-bit float: accelerometer Z
* little-endian 32-bit float: compass X
* little-endian 32-bit float: compass Y
* little-endian 32-bit float: compass Z
* Flags byte: 1 is button A, 2 is button B
* Sync terminator byte: `0x0A`, ASCII newline
