# OSCGraphRunner protocol

All numbers little-endian.

Protocol is client-driven, request/response.

Strings are represented as a u32 followed by that many bytes as a UTF-8 string.

## Authentication

Protocol starts with the client sending the password as a string.

On success, `OSCGraphEditor` is sent back as a string, else something else is sent back.

The client can now send commands:

* `store_download`: Sends back "ok", followed by:
	* u64: VM generation
	* u64: Float count
	* f64[float count]: Floats
* `cfg_upload`: Client must then send JSON as a string. Sends back "ok" or an error string. If it sends back "ok", this is followed by a u64 with the resulting VM generation.
* `cfg_download`: Sends back "ok" followed by config JSON as a string.
* `poke`: Client must then send a u64 (VM generation), a u32 (address), and an f64 (value).
