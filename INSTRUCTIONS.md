# Instructions

## Step 1: Optionally, A Network

If possible and convenient, setup a wi-fi network just between the intended Android device and the computer.

## Step 2: The Mobile App

Compile and install the Android project in `mobile`. Nothing more to it. Alone, this application is a tool to send Android sensor data over OSC, and also can be used as a remote control of sorts for various purposes, so you may find use for it.

**Importantly, the mobile app runs in the background as a "foreground service". This allows it to run while Sunlight is in use, unless an automatic power management mechanism intervenes.**

**Also-also, remember to adjust the app's power settings. This usually shows up in the Battery settings somewhere. It only needs to be able to run in the background, it doesn't need to auto-launch or anything.**

## Step 3: Preparing The Environment

Essentially, you should ensure that `$OPENHMD_FILEDEV_0` is set to something like `/run/user/1000/app/com.valvesoftware.Steam/openhmd`. You may also wish to add `mkdir -p /run/user/1000/app/com.valvesoftware.Steam/` to some 'login' or 'profile' script. This directory was chosen so that flatpak Steam can access it.

Also, because SteamVR-OpenHMD makes ominous noises about the importance of doing this, you may also wish to `export OHMD_VENDOR_OVERRIDE=Oculus` while you are there.

## Step 4: The Station

`station/station3.py` is a program to take the OSC data from the mobile app and turn it into a continually updated file containing the HMD state. It requires `python3-liblo`, but otherwise should be happy with stock Python.

Look at `station/hmd.sh` for an example; this is my personal setup. Also check the command-line options. `station/left.sh` was an attempt at a phone as a left controller, but this did not prove as useful as I had hoped due to SteamVR limitations.

You can and should take a moment to test that the station can receive data from the mobile device you are using in your planned setup.

## Step 5: Patched SteamVR-OpenHMD

The key element to bridging this across to SteamVR is a patched version of <https://github.com/ChristophHaag/SteamVR-OpenHMD>.

See [this document for the patching/install guide.](station/STEAMVR_OPENHMD_FOR_FLATPAK.md)

**The patches to OpenHMD can also be used to run software such as Monado outside of SteamVR.**

## Step 6: The Fake EDID

Download <https://github.com/Fredrum/riftOnLinux/blob/main/edid/std_rift_edid_01.bin> and keep it safe. This is an Oculus Rift CV1's EDID, which is perfect to get SteamVR to do what's needed. 
**SteamVR wants a dedicated monitor via KMS and Sunshine also can only stream a dedicated monitor via KMS.**

This now has to be put into the `/lib/firmware` of your initramfs.
This is a three-step process:

1. Copy `std_rift_edid_01.bin` to `/lib/firmware/`.
2. Copy `station/add_cv1_edid_fw` to `/etc/initramfs-tools/hooks/add_cv1_edid_fw`
3. Run `update-initramfs -u -k all`

Finally, careful misuse of `/etc/default/grub` or equivalent: `GRUB_CMDLINE_LINUX="video=HDMI-A-1:1920x1080M@60e drm.edid_firmware=HDMI-A-1:std_rift_edid_01.bin"`

Note that `HDMI-A-1` should be whatever your "donor" connector is. *The "1920x1080" here is ignored; specifying the mode here is just to ensure the port is considered connected.*

**TODO: Modifying the EDID, perhaps with debugfs for live injection? Needs to still be "Riftish" so X/Wayland leaves it alone, but after that?**

This makes it look like a Rift is *present* on the port, *even if there's no actual device there, which makes SteamVR or Monado or whatever you are using happy.

## Step 7: Sunshine, Sunshine, Test & Calibrate

Alright, so, ensure the station is running and receiving data, and start SteamVR or Monado. **You need them to be actively compositing to the monitor for this part, or capture will be impossible.**

If you haven't already, install Sunshine <https://docs.lizardbyte.dev/projects/sunshine/en/latest/> (**I use a modified version, <https://github.com/20kdc/Sunshine/tree/kdc-prod>, so that I can force IDRs, which is important because with the bandwidth limitations you have for VR you can't recover properly from errors automatically**) and set it up with Moonlight <https://f-droid.org/packages/com.limelight/>. The main goal of what you should be aiming to do here is to broadcast the "dummy monitor" to the mobile device.

If that works, now you need to do calibration. Each calibration run means shutting down SteamVR and the station, changing a constant, restarting the station, *then* SteamVR, *then* re-opening the Sunshine stream (because it stops when SteamVR disables the monitor). The goal of calibration is to set things like inter-lens distance and FOV - in fact these are what you will usually be setting.

Some notes:

* `--phys-height`: This is used to get a pretty reliable first approximation for `--lens-sep` and does other important stuff.
* `--fov`: You can tell when this is too high because UIs are unusable (because they are too small). Other than that adjust to taste.
* `--lens-sep`: Too high and you won't be able to cross-associate stuff. Too low and you'll basically be monocular.

## Step 8: Additional Equipment & Setup

This is even _less_ properly documented, but there are various mechanisms you can use in `station3.py` to eke out just a bit more flexibility.

_Whatever I am presently using at the MTPVR meetups will probably be in `aaa-setup`, as this script is how I'm going to be launching the system from now on._

* A second Android device can run "The Hand". This serves as a head position controller using multitouch to allow you to move your head in full 3D. Catch is, you can't get a hand on your gamepad while you're doing this, but it's better than the thumbstick for some social situations when just chilling. Attempting to use this for a hand controller was attempted (`left.sh`), but ran into some issues. I don't remember what said issues were.
* The `wmx` C program and script allow using a Wiimote as a head position controller. This is somewhat less flexible than the two-handed The Hand method, but should be much more convenient. Look at `wmx.sh` and `hmd-wmx.sh` for reference on this. The `+` button launches `./sunshine-pls` which if you have it setup right can force IDRs, while the `-` button triggers a recentre.

## Step 9: Room Setup

When doing SteamVR Room Setup, you should choose standing (as opposed to room-scale), and enter your eye height.